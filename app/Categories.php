<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public function subCategories() {
		return $this->hasMany('App\SubCategories','category_id');
	}

}
