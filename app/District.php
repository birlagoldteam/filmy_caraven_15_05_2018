<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'district';
    
    public $timestamps = false;
	
	protected $fillable = ['state_id','district_name'];
	
	public function states() {
		return $this->belongsTo('App\State', 'state_id', 'id');
	}
}
