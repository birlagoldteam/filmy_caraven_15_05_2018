<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyMembers extends Model
{
    //~ function memberships() {
        //~ return $this->belongsTo('Memberships', 'user_id');
    //~ }
    
    public function memberships() {
        return $this->belongsTo('App\Memberships','user_id');
    }

}
