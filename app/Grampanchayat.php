<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grampanchayat extends Model
{
    protected $table = 'grampanchayat';
    
    public $timestamps = false;
	
	protected $fillable = ['state_id','district_id','tahsil_id','panchayat_name'];
	
	public function states() {
		return $this->belongsTo('App\State', 'state_id', 'id');
	}
	public function districts() {
		return $this->belongsTo('App\District', 'district_id', 'id');
	}
	public function tehsils() {
		return $this->belongsTo('App\Tehsil', 'tahsil_id', 'id');
	}
}
