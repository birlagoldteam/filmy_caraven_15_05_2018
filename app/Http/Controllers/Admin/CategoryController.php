<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\Categories;
use Session;

class CategoryController extends Controller {
	public function __construct(){
		$this->middleware('guest:admin');
	}
	
    public function index(){
		$categories = Categories::orderBy('category_name')->paginate(10);
        return view('admin.categories.admin-categories',['categories'=>$categories]);
    }
    
    public function edit(Request $request) {
		if(empty($request->id)) {
			return view('admin.categories.admin-category-add');
		} else {
			$category = Categories::where('id', $request->id)->first();
			return view('admin.categories.admin-category-edit',['category'=>$category]);
		}
	}
	
	public function updateCategory(Request $request) {
		if(empty($request->id)) {
			$category = new Categories;
			$category['category_name'] = $request['category_name'];
			$notification = array(
				'message' => 'Category '.$request['category_name'].' has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$category = Categories::where('id', $request->id)->first();
			$old_category = $category->category_name;
			$category['category_name'] = $request['category_name'];
			$notification = array(
				'message' => 'Category '.$old_category.' to '.$request['category_name'].' has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		if($category->save()) {
			return redirect()->route('admincategories')->with($notification);
		}
	}
	
	public function delete(Request $request) {
		$category = Categories::find($request->id);
		if(isset($category)) {
			$category->delete();
			$notification = array(
				'message' => 'Category '.$category->category_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'Category has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
}
