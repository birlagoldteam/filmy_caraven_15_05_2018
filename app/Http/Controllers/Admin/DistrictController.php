<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\State;
use App\District;
use Session;

class DistrictController extends Controller {
    public function index(){
		$districts = District::with('states')->orderBy('district_name')->paginate(10);
        return view('admin.districts.admin-districts',['districts'=>$districts]);
    }
    
    public function edit(Request $request) {
		$states = State::select()->get();
		if(empty($request->id)) {
			return view('admin.districts.admin-district-add',['states'=>$states]);
		} else {
			$district = District::where('id', $request->id)->first();
			return view('admin.districts.admin-district-edit',['district'=>$district,'states'=>$states]);
		}
	}
	
	public function updateDistrict(Request $request) {
		if(empty($request->id)) {
			$district = new District;
			$district['state_id'] = $request['state_id'];
			$district['district_name'] = $request['district_name'];
			$notification = array(
				'message' => 'District '.$request['district_name'].' has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$district = District::where('id', $request->id)->first();
			$old_district = $district->district_name;
			$district['state_id'] = $request['state_id'];
			$district['district_name'] = $request['district_name'];
			$notification = array(
				'message' => 'District '.$old_district.' to '.$request['district_name'].' has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		if($district->save()) {
			return redirect()->route('admindistricts')->with($notification);
		}
	}
	
	public function delete(Request $request) {
		$District = District::find($request->id);
		if(isset($District)) {
			$District->delete();
			$notification = array(
				'message' => 'District '.$District->District_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'District has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
}
