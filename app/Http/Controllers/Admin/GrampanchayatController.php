<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\State;
use App\District;
use App\Tehsil;
use App\Grampanchayat;
use Session;

class GrampanchayatController extends Controller {
    public function index(){
		$grampanchayats = Grampanchayat::with('states','districts','Tehsils')->orderBy('panchayat_name')->paginate(10);
        return view('admin.grampanchayats.admin-grampanchayats',['grampanchayats'=>$grampanchayats]);
    }
    
    public function edit(Request $request) {
		$states = State::select()->get();
		if(empty($request->id)) {
			return view('admin.grampanchayats.admin-grampanchayat-add',['states'=>$states]);
		} else {
			$grampanchayat = Grampanchayat::where('id', $request->id)->first();
			return view('admin.grampanchayats.admin-grampanchayat-edit',['grampanchayat'=>$grampanchayat,'states'=>$states]);
		}
	}
	
	public function updateGrampanchayat(Request $request) {
		if(empty($request->id)) {
			$grampanchayat = new Grampanchayat;
			$notification = array(
				'message' => 'Grampanchayat '.$request['grampanchayat_name'].' has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$grampanchayat = Grampanchayat::where('id', $request->id)->first();
			$old_grampanchayat = $grampanchayat->grampanchayat_name;
			$notification = array(
				'message' => 'Grampanchayat has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		$grampanchayat['state_id'] = $request['state_id'];
		$grampanchayat['district_id'] = $request['district_id'];
		$grampanchayat['tahsil_id'] = $request['tahsil_id'];
		$grampanchayat['panchayat_name'] = $request['panchayat_name'];
		if($grampanchayat->save()) {
			return redirect()->route('admingrampanchayats')->with($notification);
		}
	}
	
	public function delete(Request $request) {
		$Grampanchayat = Grampanchayat::find($request->id);
		if(isset($Grampanchayat)) {
			$Grampanchayat->delete();
			$notification = array(
				'message' => 'Grampanchayat '.$Grampanchayat->Grampanchayat_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'Grampanchayat has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
	
	public function getDistricts(Request $request) {
    	$stateId = $request->state_id;
    	return static::getDistrict($stateId);
    }

    public function getTehsils(Request $request) {
    	$districtId = $request->district_id;
    	return static::getTehsil($districtId);
    }
}
