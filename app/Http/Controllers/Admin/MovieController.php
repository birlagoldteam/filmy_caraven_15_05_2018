<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\Movie;
use Session;

class MovieController extends Controller {
	public function __construct(){
		$this->middleware('guest:admin');
	}
	
    public function index(){
		$movies = Movie::orderBy('movie_name')->paginate(10);
        return view('admin.movies.admin-movies',['movies'=>$movies]);
    }
    
    public function edit(Request $request) {
		if(empty($request->id)) {
			return view('admin.movies.admin-movie-add');
		} else {
			$movie = Movie::where('id', $request->id)->first();
			return view('admin.movies.admin-movie-edit',['movie'=>$movie]);
		}
	}
	
	public function updateMovie(Request $request) {
		if(empty($request->id)) {
			$movie = new Movie;
			$notification = array(
				'message' => 'Movie '.$request['movie_name'].' has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$movie = Movie::where('id', $request->id)->first();
			$old_movie = $movie->movie_name;			
			$notification = array(
				'message' => 'Movie '.$old_movie.' to '.$request['movie_name'].' has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		$movie['movie_name'] = $request['movie_name'];
		$movie['active'] = $request['active'];
		if($movie->save()) {
			return redirect()->route('adminmovies')->with($notification);
		}
	}
	
	public function delete(Request $request) {
		$Movie = Movie::find($request->id);
		if(isset($Movie)) {
			$Movie->delete();
			$notification = array(
				'message' => 'Movie '.$Movie->Movie_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'Movie has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
}
