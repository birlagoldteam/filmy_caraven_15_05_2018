<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\State;
use App\MovieSchedule;
use App\Movie;
use Session;
use Excel;

class MovieScheduleController extends Controller {
	public function __construct(){
		$this->middleware('guest:admin');
	}
	
    public function movieschedule(){
		//$movieSchedules = MovieSchedule::with(['states','districts','tehsils','grampanchayats','movies'])->orderBy('movie_date','desc')->paginate(10);
		$movieSchedules = MovieSchedule::attendanceCountjoin()->paginate(10);
		return view('admin.movie_schedule.admin-movie-schedule',['movieSchedules'=>$movieSchedules]);
    }
    
    public function search(Request $request){
		$from_date = '';
		$to_date = '';
		$conditions = array();
		if(!empty($request->movie_from_date)) {
			$conditions[] = array('movie_date' ,'>=',$request->movie_from_date);
			$from_date = $request->movie_from_date;
		}
		if(!empty($request->movie_to_date)) {
			$conditions[] = array('movie_date' ,'<=',$request->movie_to_date);
			$to_date = $request->movie_to_date;
		}
		//DB::enableQueryLog();
		//$movieSchedules = MovieSchedule::with('attendanceCount')->where($conditions)->get();
		/* time 1.02 */
		$movieSchedules = MovieSchedule::attendanceCountjoin()->where($conditions)->paginate(10);
		//dd($movieSchedules);
		/* time 1.80 */
		/*$movieSchedules =  MovieSchedule::where($conditions)->withCount(['attendances' => function ($query) {
			$query->groupBy('movie_schedule_id');
			$query->select('movie_schedule_id', \DB::raw('count(*) as total_members'));
	   }])->get();*/

		//dd(DB::getQueryLog(),$movieSchedules);
		return view('admin.movie_schedule.admin-movie-schedule-search',['movieSchedules'=>$movieSchedules,'from_date'=>$from_date,'to_date'=>$to_date]);
    }
    
    public function edit(Request $request) {
		$states = State::select()->get();
		$movies = Movie::select()->get();
		if(empty($request->id)) {
			return view('admin.movie_schedule.admin-movie-schedule-add',['states'=>$states,'movies'=>$movies]);
		} else {
			$movieSchedules = MovieSchedule::where('id', $request->id)->first();
			return view('admin.movie_schedule.admin-movie-schedule-edit',['movieSchedules'=>$movieSchedules,'states'=>$states,'movies'=>$movies]);
		}
	}
	
	public function updateMovieSchedule(Request $request) {
		//dd($request);
		$present = MovieSchedule::where('tehsil_id','=',$request->tehsil_id)->where('movie_date','=',$request->movie_date)->where('id','!=',$request->id)->first();
		if(isset($present)) {
			$notification = array(
				'message' => 'Movie Already has been Scheduled. Please Try Again',
				'alert-type' => 'error'
			);
			return back()->with($notification);
		}
		if(empty($request->id)) {
			$movieSchedule = new MovieSchedule;
			$notification = array(
				'message' => 'Movie Schedule has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$movieSchedule = MovieSchedule::where('id', $request->id)->first();
			$notification = array(
				'message' => 'Movie Schedule has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		$movieSchedule['state_id'] = $request['state_id'];
		$movieSchedule['district_id'] = $request['district_id'];
		$movieSchedule['tehsil_id'] = $request['tehsil_id'];
		$movieSchedule['grampanchayat_id'] = $request['grampanchayat_id'];
		$movieSchedule['movie_id'] = $request['movie_id'];
		$movieSchedule['movie_date'] = $request['movie_date'];
		$movieSchedule['movie_time'] = $request['movie_time'];
		if($movieSchedule->save()) {
			return redirect()->route('adminmovieschedules')->with($notification);
		} else {
			$notification = array(
				'message' => 'Movie Schedule has not been Added Please Try Again',
				'alert-type' => 'error'
			);
			return redirect()->route('adminmovieschedules')->with($notification);
		}
	}
	
	public function attendances(Request $request){
		$attendances = MovieSchedule::with(['attendances','movies','grampanchayats'])->where('id','=',$request->id)->first();
		//dd($attendances);
        return view('admin.movie_schedule.admin-movie-schedule-attendances',['attendances'=>$attendances]);
    }
	
	public function delete(Request $request) {
		$MovieSchedule = MovieSchedule::find($request->id);
		if(isset($MovieSchedule)) {
			$MovieSchedule->delete();
			$notification = array(
				'message' => 'MovieSchedule '.$MovieSchedule->MovieSchedule_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'MovieSchedule has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
	
	public function getDistricts(Request $request) {
    	$stateId = $request->state_id;
    	return static::getDistrict($stateId);
    }

    public function getTehsils(Request $request) {
    	$districtId = $request->district_id;
    	return static::getTehsil($districtId);
    }
    
    public function getgrampanchayats(Request $request) {
    	$tehsilId = $request->tehsil_id;
    	return static::getgrampanchayat($tehsilId);
    }
    
    public function schedulereportexport(Request $request){
		$data = array();
		$conditions = array();
		if(!empty($request->movie_from_date)) {
			$conditions[] = array('movie_date' ,'>=',$request->movie_from_date);
		}
		if(!empty($request->movie_to_date)) {
			$conditions[] = array('movie_date' ,'<=',$request->movie_to_date);
		}
		$data['members'] = MovieSchedule::attendanceCountjoin()->where($conditions)->get();
		//dd($data);
		$filename = "schedule-data";
		$data['total-user'] = array();
		$k = 0;
		/*foreach($data['members'] as $datanew){
            $data['total-user'][$k]['movie_id'] = $datanew->movie_id;
            $data['total-user'][$k]['state_id'] = $datanew->state_id;
            $data['total-user'][$k]['district_id'] = $datanew->district_id;
            $data['total-user'][$k]['movie_date'] = $datanew->movie_date;
            $data['total-user'][$k]['movie_time'] = $datanew->movie_time;
            $k++;
        }*/
		Excel::create($filename.date("d-m-Y"), function($excel) use($data) {
			$excel->sheet('Sheetname', function($sheet) use($data) {
				$datasheet = array();
				$datasheet[0] = array('Movie','State','District','Tehsil','Grampanchayat','Movie Date','Movie Time','Total Members');
				$i=1;
				foreach($data['members'] as $datanew){
					$datasheet[$i] = array(
						$datanew['movie_name'],
						$datanew['state_name'],  
						$datanew['district_name'],  
						$datanew['tehsil_name'],
						$datanew['panchayat_name'],
						date('d-m-Y',strtotime($datanew['movie_date'])),
						date('g:i a', strtotime($datanew->movie_time)),
						$datanew['total_members'],
					);
					$i++;
				}
				$sheet->fromArray($datasheet);
			});

        })->download('csv');
		
		
    }
}
