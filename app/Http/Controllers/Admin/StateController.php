<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\State;
use Session;

class StateController extends Controller {
	public function __construct(){
		$this->middleware('guest:admin');
	}
	
    public function index(){
		$states = State::orderBy('state_name')->paginate(10);
        return view('admin.states.admin-states',['states'=>$states]);
    }
    
    public function edit(Request $request) {
		if(empty($request->id)) {
			return view('admin.states.admin-state-add');
		} else {
			$state = State::where('id', $request->id)->first();
			return view('admin.states.admin-state-edit',['state'=>$state]);
		}
	}
	
	public function updateState(Request $request) {
		if(empty($request->id)) {
			$state = new State;
			$state['state_name'] = $request['state_name'];
			$notification = array(
				'message' => 'State '.$request['state_name'].' has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$state = State::where('id', $request->id)->first();
			$old_state = $state->state_name;
			$state['state_name'] = $request['state_name'];
			$notification = array(
				'message' => 'State '.$old_state.' to '.$request['state_name'].' has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		if($state->save()) {
			return redirect()->route('adminstates')->with($notification);
		}
	}
	
	public function delete(Request $request) {
		$state = State::find($request->id);
		if(isset($state)) {
			$state->delete();
			$notification = array(
				'message' => 'State '.$state->state_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'State has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
}
