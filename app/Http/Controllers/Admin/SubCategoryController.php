<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\SubCategories;
use App\Categories;
use Session;

class SubCategoryController extends Controller {
	public function __construct(){
		$this->middleware('guest:admin');
	}
	
    public function index(){
		$subcategories = SubCategories::with('categories')->orderBy('sub_category_name')->paginate(10);
        return view('admin.sub_categories.admin-sub-categories',['subcategories'=>$subcategories]);
    }
    
    public function edit(Request $request) {
    	$categories = Categories::select()->get();
		if(empty($request->id)) {
			return view('admin.sub_categories.admin-sub-category-add',['categories'=>$categories]);
		} else {
			$subCategory = SubCategories::where('id', $request->id)->first();
			return view('admin.sub_categories.admin-sub-category-edit',['subCategory'=>$subCategory,'categories'=>$categories]);
		}
	}
	
	public function updateSubCategory(Request $request) {
		if(empty($request->id)) {
			$subCategory = new SubCategories;
			$notification = array(
				'message' => 'Sub-Category '.$request['sub_category_name'].' has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$subCategory = SubCategories::with('categories')->where('id', $request->id)->first();
			$old_subCategory = $subCategory->sub_category_name;
			$notification = array(
				'message' => 'Sub-Category '.$old_subCategory.' to '.$request['sub_category_name'].' has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		$subCategory['category_id'] = $request['category_id'];
		$subCategory['sub_category_name'] = $request['sub_category_name'];
		if($subCategory->save()) {
			return redirect()->route('adminsubcategories')->with($notification);
		}
	}
	
	public function delete(Request $request) {
		$subCategory = SubCategories::find($request->id);
		if(isset($subCategory)) {
			$subCategory->delete();
			$notification = array(
				'message' => 'Sub-Category '.$subCategory->sub_category_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'Sub-Category has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
}
