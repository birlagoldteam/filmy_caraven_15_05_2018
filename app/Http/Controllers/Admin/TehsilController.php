<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use App\State;
use App\District;
use App\Tehsil;
use Session;

class TehsilController extends Controller {
    public function index(){
		$tehsils = Tehsil::with('states','districts')->orderBy('tehsil_name')->paginate(10);
        return view('admin.tehsils.admin-tehsils',['tehsils'=>$tehsils]);
    }
    
    public function edit(Request $request) {
		$states = State::select()->get();
		$districts = District::select()->get();
		if(empty($request->id)) {
			return view('admin.tehsils.admin-tehsil-add',['states'=>$states,'districts'=>$districts]);
		} else {
			$tehsil = Tehsil::where('id', $request->id)->first();
			return view('admin.tehsils.admin-tehsil-edit',['tehsil'=>$tehsil,'states'=>$states,'districts'=>$districts]);
		}
	}
	
	public function updateTehsil(Request $request) {
		if(empty($request->id)) {
			$tehsil = new Tehsil;
			$notification = array(
				'message' => 'Tehsil '.$request['tehsil_name'].' has been Added Successfully',
				'alert-type' => 'success'
			);
		} else {
			$tehsil = Tehsil::where('id', $request->id)->first();
			$old_tehsil = $tehsil->tehsil_name;
			$notification = array(
				'message' => 'Tehsil has been Updated Successfully',
				'alert-type' => 'info'
			);
		}
		$tehsil['state_id'] = $request['state_id'];
		$tehsil['district_id'] = $request['district_id'];
		$tehsil['tehsil_name'] = $request['tehsil_name'];
		$tehsil['mobile_no'] = $request['mobile_no'];
		if($tehsil->save()) {
			return redirect()->route('admintehsils')->with($notification);
		}
	}
	
	public function delete(Request $request) {
		$Tehsil = Tehsil::find($request->id);
		if(isset($Tehsil)) {
			$Tehsil->delete();
			$notification = array(
				'message' => 'Tehsil '.$Tehsil->Tehsil_name.' has been deleted',
				'alert-type' => 'error'
			);
		} else {
			$notification = array(
				'message' => 'Tehsil has not been Deleted Please Try Again',
				'alert-type' => 'warning'
			);
		}
		return back()->with($notification);
	}
}
