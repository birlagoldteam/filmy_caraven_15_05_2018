<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    
    public function __construct(){
        $this->middleware('guest:admin')->except(['showloginform','admin_login']);
    }

    public function showloginform(){
		if (Auth::guard('admin')->check()) {
			return redirect()->route('admindashboard');
		}
        return view('admin.auth.login');
    }

    public function admin_login(Request $request){
    	if (Auth::guard('admin')->attempt(['email'=>$request['username'],'password'=>$request['password']])){
			return redirect()->route('admindashboard');	
        }
        $data = ['msg'=>'Invalid Login Credentials !!!'];
          return redirect()->back()->with($data);
        //return redirect()->back()->with($request->except('password'));
  
    }
    
    public function dashboard() {
        
		return view('admin.dashboard.admin-dashboard');
	}


    public function logout(Request $request) {  
       
        Auth::guard('admin')->logout();
        return redirect()->route('admin-login',['login']);
    }
}
