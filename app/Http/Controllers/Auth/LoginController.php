<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','showLoginForm','login');
    }

    public function login(Request $request){
        /*$this->validate($request, [
            'email' => 'required|numeric|size:10',
            'password' => 'required',
        ]);*/
        if (Auth::attempt(['mobile'=>$request['email'],'password'=>$request['password']])){
            return redirect()->intended(route('membershipregistration'));  
        }
        return redirect()->route('login')->with(['msg'=>'Invalid Login or Password!!!']);  
    }
}   
