<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class NgoLoginController extends Controller {

	public function __construct(){
		$this->middleware('guest:ngo')->except(['showloginform','login']);
	}
	
	
    public function showloginform(){
		if (Auth::guard('ngo')->check()) {
			return redirect()->route('ngodashboard');
		}
		return view('auth.ngo-login');
    }

    public function login(Request $request){
    	//$this->validate('')
		if (Auth::guard('ngo')->attempt(['email'=>$request['email'],'password'=>$request['password']])){
			return redirect()->intended(route('addngodetails'));	
        }
        return redirect()->back()->with($request->except('password'));
    }
}
