<?php

namespace App\Http\Controllers;
use App\Ngo;
use App\NgoDetails;
use App\EmailOtp;
use Hash;
use DateTime;
use Validator;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Otp;


class NgoHomeController extends Controller
{
	
	public function __construct(){		
		$this->middleware('guest:ngo')->except('demo','emailvalidatengo','addngoregister','otpngoverify','check_otp','checkemail','checkemailotp');
	}
	
    public function demo(){
        return view('ngo.ngoregister');
    }
    public function emailvalidatengo(Request $request) { 
        $email= $request['email'];
        $emailcheck = EmailOtp::where('email', $email)
                        ->select("email")
                        ->first(); 

if(!empty($emailcheck)){
            return ['success'=>false,'error'=>'Email Id Is  Already Exist!!!!!!!!','data'=>[]];
        }
        return 1; 
                                 
       
    }
    public function otpngoverify(Request $request) {
        $email = $request['email'];
        $num_str = sprintf("%06d",mt_rand(1,999999));
        $data['to_name']= $request['name']; 
        $data['messages'] = "OTP for fimly Caravan $num_str";
        $data['to_mail'] =  $email;        
        $data['from_email'] = "info@kssboxoffice.com";
        $data['from_name'] = "KSS Box Office";
        $data['subject'] = "OTP for Password Reset";
        //$this->sendemail($data);

        $opt = EmailOtp::where(['email'=>$email]);
        $check = clone ($opt);
        if(!empty($opt->first())){
            return $check->update(['otp'=>$num_str]);
        }

        $emailotpObj = new EmailOtp();
        $emailotpObj->email = $request['email'];
        $emailotpObj->otp = $num_str;
        $emailotpObj->save();


     


        return 1;
    }
   public function  addngoregister(Request $request) { 
        
        $otpcheck = EmailOtp::where('otp', $request['otp'])
                        ->select("otp")
                        ->first();                             
        if(empty($otpcheck)){
            return ['success'=>false,'error'=>'Otp Is Incorrect, Please Re-enter Otp','data'=>[]];        
        } 
        $ngoObj = new Ngo();
        $ngoObj->name = $request['name'];
        $ngoObj->email = $request['email'];
        $ngoObj->password = bcrypt($request['password']);
        $ngoObj->mobile = $request['mobile'];        
        $ngoObj->save();
       // dd($ngoObj);
        Auth::guard('ngo')->loginUsingId($ngoObj->id);
        return redirect()->route('ngo-registration');
     
  
    }

    public function check_otp(Request $request) {
        $otp = $request['otp'];           
        $otpcheck = Otp::where('otp', $otp)
                        ->select("otp")
                        ->first();                             
        if(empty($otpcheck)){
            return ['success'=>false,'error'=>'Otp Is Incorrect, Please Re-enter Otp','data'=>[]];        
        }
        $registerObj = $this->create($request->all());
        Auth::loginUsingId($registerObj->id);
        return ['success'=>true,'error'=>'','data'=>1];
    }

     public function  addapingoregister(Request $request) {
      $email= $request['email'];      
      $emailecheck = Ngo::where('email', $email)
                        ->select("email")
                        ->first();                             
       if(!empty($emailecheck)){
            return ['success'=>false,'error'=>'Email Id Is  Already Exist!!!!!!!!','data'=>[]];
        } 
        
        $ngoObj = new Ngo();
        $ngoObj->name = $request['name'];
        $ngoObj->email = $request['email'];
        $ngoObj->password = bcrypt($request['password']);
        $ngoObj->mobile = $request['mobile'];        
        $ngoObj->save();
        //print_r($ngoObj); dd('mahima');
        return ['success'=>true,'error'=>'','data'=>['id'=>$ngoObj->id]];  
       
    }  
    public function index(){
        return view('home');
    }


    public function education (Request $request) {

        $educations = config('custome');      
        foreach ($educations as $key => $value) {
          echo "<pre>";
          print_r($value);
          
          echo "</pre>";
          echo $value['SSC/Matriculation'];
        }


    }


public function checkemail(Request $request){
        $email = $request->email;
        $flag = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($flag===false){
            return ['success'=>false,'error'=>'Invalid Email Id!!','data'=>""];
        }
        $data = Ngo::where('email',$email)
                    ->first();
        if(empty($data)){
            return ['success'=>false,'error'=>'Email Id does Not Exists!!!','data'=>""];
        }
        Session::put('ngo_id',$data->id);
        $num_str = sprintf("%06d",mt_rand(1,999999));    
        $this->opt(['email'=>$email,'otp'=>$num_str]);
        $data['messages'] = "OTP for fimly Caravan $num_str";
        $data['to_mail'] = $email;
        $data['to_name'] = $data->name;
        $data['from_email'] = "info@kssboxoffice.com";
        $data['from_name'] = "KSS Box Office";
        $data['subject'] = "OTP for Password Reset";
        $this->sendemail($data);
        return ['success'=>true,'error'=>'','data'=>""];
    }

 public function checkemailotp(Request $request){
        $email = $request->email;
        $otp = $request->otp;
        $data = Otp::where('email',$email)
                    ->where('otp',$otp)
                    ->first();
        if(empty($data)){
            return ['success'=>false,'error'=>'Invalid Otp','data'=>""];
        }
        return ['success'=>true,'error'=>'','data'=>""];
    }

    public function forgotpassword(){
        if(Session::has('ngo_id')){
            return view('auth.ngo-forgot-password');
        }else{
            return redirect()->route('ngo-login');
        }
    }

    public function resetpassword(Request $request){
        $id = Session::get('ngo_id');
        $password = $request->password;
        Ngo::where('id',$id)
                  ->update(['password'=>\Hash::make($password)]);
        return redirect()->route('ngo-login');
    }
    
    public function dashboard() {
		$ngo = NgoDetails::where('user_id',Auth::guard('ngo')->user()->id)->first();
		return view('ngo.ngodashboard',['ngo'=>$ngo]);
	}
}
