<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MovieSchedule;
use App\MovieAttendance;

class SmsController extends Controller
{
    public function movieschedule(Request $request)
	{
		/*1)get sms content
		  3)if mobile no already exist for same date
		  4)no record exist for movie;*/

		$mobile = substr($request['caller_id'],-10);
		$tehsilmobile = substr($request['dispnumber'],-10);
		$result=MovieAttendance::where('mobile',$mobile)
							   ->where('schedule_date',date('Y-m-d'))
					  		   ->first();

		if(empty($result)){
			$data = MovieSchedule::whereHas('tehsils',function($q) use ($tehsilmobile){
						$q->where('mobile_no',$tehsilmobile);
						$q->where('movie_date',date('Y-m-d'));
					})->with('movies')
					->first();
			if(!empty($data)){
				$serial_no =  MovieAttendance::where('movie_schedule_id',$data->id)
							   				 ->count();
				$obj= new MovieAttendance();
				$obj->mobile = $mobile;
				$obj->movie_schedule_id = $data->id;
				$obj->schedule_date = date('Y-m-d');
				$obj->serial_no = $serial_no+1;
				$obj->save();	
				$movie_time = date('h:i A',strtotime($data->movie_time));
				$movie_date = $data->movie_date;
				$sms= "We Welcome you for ".$data->movies->movie_name." on ". date('d-m-Y',strtotime($movie_date))." at ".$movie_time.". Your serial no is ".($serial_no+1);
				var_dump($sms);
				$this->sendsms($mobile,$sms);
				return;
			}
			return;
		}
		return;
		
	}
}
