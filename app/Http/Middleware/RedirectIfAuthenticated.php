<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		if (Auth::guard($guard)->check()) {
			return $next($request);
        }else{
        	$current_route = (string)Route::currentRouteName();
        	
			if($guard===null || $guard=='users'){
				return redirect()->route('welcome');
			}elseif($guard=='ngo'){
				
				if($current_route=== 'ngo-registration'){
					return $next($request);
				}
				return redirect()->route('ngo-login');
			}elseif($guard=='admin'){
				return redirect()->route('admin-login',['login']);
			}else{

				return redirect('/home');
			}
		}
        
        
    }
}
