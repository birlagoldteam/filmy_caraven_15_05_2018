<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memberships extends Model
{
    //
        //protected $fillable = ['name' ];
        
        protected $table = 'memberships';

		protected $primaryKey = 'id';
        
        //~ function family_members() {
			//~ //return $this->hasMany('App\FamilyMembers', 'foreign_key', 'user_id');
			//~ return $this->hasMany('FamilyMembers', 'user_id');
		//~ }
		
		public function familyMembers() {
			return $this->hasMany('App\FamilyMembers', 'user_id', 'user_id');
		}

}
