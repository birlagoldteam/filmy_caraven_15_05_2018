<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = 'movies';
    
    public $timestamps = true;
	
	protected $fillable = ['movie_name'];

	public function moviesschedule(){
		return $this->hasMany('App\MovieSchedule', 'movie_id', 'id');
	}
}
