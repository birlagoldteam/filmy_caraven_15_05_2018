<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieAttendance extends Model
{
    protected $table = 'movie_attendance';
	public $timestamps = true;
	protected $primaryKey = 'id';
}
