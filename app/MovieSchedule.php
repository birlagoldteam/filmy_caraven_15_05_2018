<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieSchedule extends Model
{
    protected $table = 'movie_schedule';
    
    public $timestamps = true;
	
	//protected $fillable = ['state_id','district_name'];
	
	public function states() {
		return $this->belongsTo('App\State', 'state_id', 'id');
	}
	
	public function districts() {
		return $this->belongsTo('App\District', 'district_id', 'id');
	}
	
	public function tehsils() {
		return $this->belongsTo('App\Tehsil', 'tehsil_id', 'id');
	}
	
	public function grampanchayats() {
		return $this->belongsTo('App\Grampanchayat', 'grampanchayat_id', 'id');
	}

	public function movies() {
		return $this->belongsTo('App\Movie', 'movie_id', 'id');
	}
	
	public function attendances() {
		return $this->hasMany('App\MovieAttendance', 'movie_schedule_id', 'id');
	}
	
	//this is for reference
	public static function attendanceCountjoin() {
		return static::join('movies', 'movies.id', '=', 'movie_schedule.movie_id')
					->join('state', 'state.id', '=', 'movie_schedule.state_id')
					->join('district', 'district.id', '=', 'movie_schedule.district_id')
					->join('tehsil', 'tehsil.id', '=', 'movie_schedule.tehsil_id')
					->join('grampanchayat', 'grampanchayat.id', '=', 'movie_schedule.grampanchayat_id')
					->leftjoin('movie_attendance', 'movie_attendance.movie_schedule_id', '=', 'movie_schedule.id')
					
					->groupBy('movie_schedule.id','movie_schedule_id','movie_date','movie_time','movies.movie_name','state.state_name','district.district_name','tehsil.tehsil_name','grampanchayat.panchayat_name')
					->select('movie_schedule.id','movie_schedule_id','movie_date','movie_time','movies.movie_name','state.state_name','district.district_name','tehsil.tehsil_name','grampanchayat.panchayat_name',\DB::raw('sum(case when movie_attendance.id is null then 0 else 1 end) as total_members'));
	}
	
	public function attendanceCount(){
		
	}

}
