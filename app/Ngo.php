<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Ngo extends Authenticatable
{
    use Notifiable;

    protected $guard= 'ngo';

    protected $table = 'ngo_registration';
	
	protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
