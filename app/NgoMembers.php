<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NgoMembers extends Model
{
	protected $table = 'ngo_members';
	
	protected $primaryKey = 'id';
    
    public function ngoDetails() {
        return $this->belongsTo('App\NgoDetails','user_id');
    }
}
