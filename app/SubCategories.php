<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model

{
	protected $table = 'sub_categories';
	
	protected $fillable = ['category_id','sub_category_name'];
	
	public function categories() {
		return $this->belongsTo('App\Categories', 'category_id', 'id');
	}
}

