<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tehsil extends Model
{
    protected $table = 'tehsil';
    
    public $timestamps = true;
	
	protected $fillable = ['state_id','district_id','tehsil_name'];
	
	public function states() {
		return $this->belongsTo('App\State', 'state_id', 'id');
	}
	public function districts() {
		return $this->belongsTo('App\District', 'district_id', 'id');
	}
}
