<?php 
return[
	'EDUCATIONS' => array("SSC/10th" => "SSC/10th",'HSC/12th'=>'HSC/12th','Graduate'=>'Graduate','Post-Graduate'=>'Post-Graduate','Other'=>'Other'),

    'EMPLOYMENT'=> array(
		'Employed' =>'Employed',
		'UnEmployed'=> 'UnEmployed',
		'Retired'=>'Retired',
    	'Self Employed'=>'Self Employed',
    	'Business'=>'Business',
    	'Government Service'=>'Government Service',
    	'Other'=>'Other'
    ),

    'VEHICLE' => array(
		'Two Wheeler' => 'Two Wheeler',
		'Four Wheeler'=>'Four Wheeler',
		'Heavy Vehicle'=>'Heavy Vehicle',
        'Three Wheeler'=>'Three Wheeler',
        'Other'=>'Other'
	),
    
    'ANYLONGTERM' => array('Allergy/anaphylaxis','Asthma',
    	'Atrial fibrillation','Amnesia',
    	'Chronic kidney disease','Anaemia','Angina'
    	,'Chronic pain','Depression',
    	'Anxiety and stress disorders',' Epilepsy',
    	'Diabetes','Hypertension','Blood disorders',
    	'Hepatitis B','Hepatitis C','HIV','Cancer',
    	'Migraine','Eczema','Obesity',
    	'Sleep disorders','Urinary Incontinence'),

	'LONGTERMDISEASE' => array(
		'Allergy/anaphylaxis' => 'Allergy/anaphylaxis',
		'Asthma' => 'Asthma',
		'Atrial fibrillation' => 'Atrial fibrillation',
		'Amnesia' => 'Amnesia',
		'Chronic kidney disease' => 'Chronic kidney disease',
		'Anaemia' => 'Anaemia',
		'Angina' => 'Angina',
		'Chronic pain' => 'Chronic pain',
		'Depression' => 'Depression',
		'Anxiety and stress disorders' => 'Anxiety and stress disorders',
		'Epilepsy' => 'Epilepsy',
		'Diabetes' => 'Diabetes',
		'Hypertension' => 'Hypertension',
		'Blood disorders' => 'Blood disorders',
		'Hepatitis B' => 'Hepatitis B',
		'Hepatitis C' => 'Hepatitis C',
		'HIV' => 'HIV',
		'Cancer' => 'Cancer',
		'Migraine' => 'Migraine',
		'Eczema' => 'Eczema',
		'Obesity' => 'Obesity',
		'Sleep disorders' => 'Sleep disorders',
		'Urinary Incontinence' => 'Urinary Incontinence'
	),

    'LANDPROPERTY' => array(
		'Recreational' => 'Recreational - Fun, Non-Essentials like Parks',
		'Transport'=>'Transport - Roads, Railways, and Airports',
		'Agricultural'=>'Agricultural - farmland',
					'Residential' => 'Residential - Housing',
		'Commercial'=>'Commercial – Businesses, Factories and Shops',
		'Others' => 'Others'
	),  
	  
    'ExistingLoan' => array(
		'Housing Loan' => 'Housing Loan',
		'Personal Loan'=>'Personal Loan',
		'Agricultural Loan'=>'Agricultural Loan',
		'Study Loan'=>'Study Loan',
		'Others'=>'Others'
	),

    'Insurance' =>  array(
		'Life Insurance' =>'Life Insurance',
		'General Insurance'=>'General Insurance',
		'Health Insurance'=>'Health Insurance',
		'Others'=>'Others'
	),

	'RELATIONSHIP' =>  array(
		'Mother' =>'Mother',
		'Father'=>'Father',
		'Brother'=>'Brother',
		'Sister'=>'Sister',
		'Wife'=>'Wife',
		'Husband'=>'Husband',
		'Son'=>'Son',
		'Daughter'=>'Daughter',
		'Niece'=>'Niece',
		'Nephew'=>'Nephew',
		'Grandfather'=>'Grandfather',
		'Grandmother'=>'Grandmother',
		'Grandson'=>'Grandson',
		'Granddaughter'=>'Granddaughter',
		'Mother-in-law'=>'Mother-in-law',
		'Father-in-law'=>'Father-in-law',
		'Sister-in-law'=>'Sister-in-law',
		'Brother-in-law'=>'Brother-in-law',
	),

	'Languages' =>  array('English'=>'English','Hindi'=>'Hindi','Regional'=>'Regional'),
	
	'ngoRegisterFor' => array(
		'NGO Aggregators'=>'NGO Aggregators',
		'VO - Voluntary Organisation'=>'VO - Voluntary Organisation',
		'CSR Organisations'=>'CSR Organisations',
		'Philanthropic individuals/Groups'=>'Philanthropic individuals/Groups',
		'Govt./Semi-Govt. Associates'=>'Govt./Semi-Govt. Associates'
	),
	
	'ngoRegisteredwith' => array(
		'Registrar of Societies' => 'Registrar of Societies',
		'Registrar of Companies' => 'Registrar of Companies',
		'Others' => 'Others'
	),

];
?>
