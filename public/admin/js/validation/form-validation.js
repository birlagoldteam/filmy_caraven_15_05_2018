var FormValidation = function () {
	
    var districtForm = function () {
        var form1 = $('#districtForm');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                district_name: {
                    required: true,
                },
            },
			messages: {
                district_name: {
                    required: "District Name Required"
                },
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                checkotp();
            }
        });
    }
    
    var tehsilForm = function () {
        var form2 = $('#tehsilForm');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);
        form2.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                tehsil_name: {
                    required: true,
                },
            },
			messages: {
                tehsil_name: {
                    required: "Tehsil Name Required"
                },
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form2) {
                checkotp();
            }
        });
    }
    
    var movieForm = function () {
        var form3 = $('#movieForm');
        var error3 = $('.alert-danger', form3);
        var success3 = $('.alert-success', form3);
        form3.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                movie_name: {
                    required: true,
                },
            },
			messages: {
                movie_name: {
                    required: "Movie Name Required"
                },
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success3.hide();
                error3.show();
                App.scrollTo(error3, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form3) {
                checkotp();
            }
        });
    }
    
    var movieScheduleForm = function () {
        var form4 = $('#movieScheduleForm');
        var error4 = $('.alert-danger', form4);
        var success4 = $('.alert-success', form4);
        form4.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                state_id: {
                    required: true,
                },
                district_id: {
                    required: true,
                },
                tehsil_id: {
                    required: true,
                },
                grampanchayat_id: {
                    required: true,
                },
                movie_id: {
                    required: true,
                },
                movie_date: {
                    required: true,
                },
                movie_time: {
                    required: true,
                },
            },
			messages: {
                state_id: {
                    required: "State is Required"
                },
                district_id: {
                    required: "District is Required"
                },
                tehsil_id: {
                    required: "Tehsil is Required"
                },
                grampanchayat_id: {
                    required: "Grampanchayat is Required"
                },
                movie_id: {
                    required: "Movie is Required"
                },
                movie_date: {
                    required: "Date is Required"
                },
                movie_time: {
                    required: "Time is Required"
                },
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success4.hide();
                error4.show();
                App.scrollTo(error4, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form4) {
                checkotp();
            }
        });
    }
    
	return {
        init: function () {
            districtForm();
            tehsilForm();
            movieForm();
            movieScheduleForm();
        }
    };
}();
