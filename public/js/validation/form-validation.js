var FormValidation = function () {
	
    var register = function () {
        var form1 = $('#registerform');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                name: {
                    required: true,
                },
                email: {
                    email:true,
                }, 
                mobile : {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10,
                },              
                password: {
                    required: true,
                    minlength: 6,
                },
                confirmpassword:{
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
			messages: {
                name: {
                    required: "Name is Required!!!"
                },
                email:{
                    email:"Enter valid Email id!!!"
                },
                mobile:{
                    required: "Mobile No is Required",
                    number: "Enter Valid Mobile No",
                    minlength: "Mobile No should be 10 character",
                    maxlength: "Mobile No should be 10 character",
                },
                password: {
                    required: "Password is Required!!!",
                    minlength:"Password should be atleast 6 character!!!"
                },
                confirmpassword: {
                    required: "Confirm Password is Required!!!",
                    minlength:"Password should be atleast 6 character!!!"
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                checkotp();
            }
        });
    }

    var loginform = function () {
        var form2 = $('#loginform');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);
        form2.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                email: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10,
                },               
                password: {
                    required: true,
                },

            },
              messages: { 
                email: {
                    required: "Mobile No is Required",
                    number: "Enter Valid Mobile No",
                    minlength: "Mobile No should be 10 character",
                    maxlength: "Mobile No should be 10 character",
                },
                password: {
                    required: "Please Enter password !!!"
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
              form.submit();
            }
        });
    }

    var membershipForm = function () {
        var form3 = $('#membershipForm');
        var error3 = $('.alert-danger', form3);
        var success3 = $('.alert-success', form3);
        form3.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                aadhar_number: {
                    required: true,
                    minlength: 12,
                    maxlength: 12,
                    number: true
                },               
                agree: {
                    required: true,
                },
                declaration: {
                    required: true,
                },

            },
              messages: { 
                aadhar_number: {
                    required: "Aadhar No. is Required",
                    minlength: "Aadhar No should be 12 digit",
                    maxlength: "Aadhar No should be 12 digit",
                    number: "Aadhar No should be Numeric"
                   
                },
                agree: {
                    required: "Please Accept Terms and Conditions",
                },
                declaration: {
                    required: "Please accept Declarations",
                },
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success3.hide();
                error3.show();
                App.scrollTo(error3, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
              form.submit();
            }
        });
    }

     var ngoregister = function () {
        var form4 = $('#ngoregister');
        var error4 = $('.alert-danger', form4);
        var success4 = $('.alert-success', form4);
        form4.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                name: {
                    required: true,
                   
                },
                email: {
                    required: true,
                    email: true                   
                }, 
                mobile : {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10,
                },              
                password: {
                    required: true,
                    minlength: 6,
                },
                confirmpassword:{
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
			messages: {
                name: {
                    required: "Name is Required!!!"
                },
                email:{
                    email:"Enter valid Email id!!!"
                },
                mobile:{
                    required: "Mobile No is Required",
                    number: "Enter Valid Mobile No",
                    minlength: "Mobile No should be 10 character",
                    maxlength: "Mobile No should be 10 character",
                },
                password: {
                    required: "Password is Required!!!",
                    minlength:"Password should be atleast 6 character!!!"
                },
                confirmpassword: {
                    required: "Confirm Password is Required!!!",
                    minlength:"Password should be atleast 6 character!!!"
                }
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success4.hide();
                error4.show();
                App.scrollTo(error4, -200);
            },

            highlight: function (element) {
                $(element).closest('.ngore').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.ngore').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.ngore').removeClass('has-error');
            },
            submitHandler: function (form4) {
               checkotp(); 
            }
             
        });
    }
    
    var ngoRegistrationForm = function () {
        var form5 = $('#ngoRegistrationForm');
        var error5 = $('.alert-danger', form5);
        var success5 = $('.alert-success', form5);
        form5.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                unique_id: {
                    required: true,
                   
                },
                
            },
			messages: {
                unique_id: {
                    required: "Name is Required!!!"
                },
            }, 
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success5.hide();
                error5.show();
                App.scrollTo(error5, -200);
            },

            highlight: function (element) {
                $(element).closest('.ngore').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.ngore').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.ngore').removeClass('has-error');
            },
            submitHandler: function (form5) {
               checkotp(); 
            }
             
        });
    }


	return {
        init: function () {
            ngoregister();
			register();
            loginform();
            membershipForm();
            ngoRegistrationForm();
        }
    };
}();
