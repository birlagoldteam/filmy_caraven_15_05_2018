@extends('admin.layouts.app')
@section('page_title')
    Categories
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Add Category</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Add Category </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="categoryForm" name="caregoryForm" action="{{ route('admin-update-category') }}" method="POST" role="form">
							{{ csrf_field() }}
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<input type="hidden" value="" name="id">
										<input type="text" class="form-control" placeholder="Category Name" name="category_name" id="category_name">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('admincategories') }}" class="btn default cancel">Cancel</a>
								<input type="submit"  value="Submit" class="btn green" data-loading-text="Loading...">
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{URL::asset('public/admin/js/validation/jquery.validate.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/form-validation.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/app.js')}}"></script>
    <script>
		$(document).ready(function() {
			App.init();
			FormValidation.init();
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
		function formsubmit(form){
			form.submit();
		}
    </script>
    <!-- END CONTENT BODY -->
@endsection    
