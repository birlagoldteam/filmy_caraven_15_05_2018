@extends('admin.layouts.app')
@section('page_title')
    Categories
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Edit Category</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Edit Category {{$category->category_name}} </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="categoryForm" name="categoryForm" action="{{ route('admin-update-category') }}" method="POST" role="form">
							{{ csrf_field() }}
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<input type="hidden" value="{{$category->id}}" name="id">
										<input type="text" value="{{$category->category_name}}" class="form-control" placeholder="State Name" name="category_name" id="category_name">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('admincategories') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
    </script>
@endsection    
