@extends('admin.layouts.app')
@section('page_title')
    States
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Edit District</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Edit District {{$district->district_name}} </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
							<!--<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
							<a href="" class="reload" data-original-title="" title=""> </a>
							<a href="" class="remove" data-original-title="" title=""> </a>-->
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="districtForm" name="districtForm" action="{{ route('admin-update-district') }}" method="POST" role="form">
							{{ csrf_field() }}
							<div class="form-body">
								<div class="row">
									<div class="col-md-6">
										<input type="hidden" value="{{$district->id}}" name="id" name="id">
										<select class="form-control" name="state_id" id="state_id">
											<option>Select State</option>
											@if(!empty($states))
												@foreach($states as $key=>$value)
													<option value="{{ $value->id }}" @if($district->state_id == $value->id) selected @endif>{{ $value->state_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-6">
										<input type="text" value="{{$district->district_name}}" class="form-control" placeholder="District Name" name="district_name" id="district_name">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('admindistricts') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
    </script>
@endsection    
