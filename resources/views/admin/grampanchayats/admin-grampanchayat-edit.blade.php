@extends('admin.layouts.app')
@section('page_title')
    States
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Edit Grampanchayat</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Edit Grampanchayat {{$grampanchayat->grampanchayat_name}} </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
							<!--<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
							<a href="" class="reload" data-original-title="" title=""> </a>
							<a href="" class="remove" data-original-title="" title=""> </a>-->
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="grampanchayatForm" name="grampanchayatForm" action="{{ route('admin-update-grampanchayat') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" value="{{$grampanchayat->id}}" name="id" name="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-3">
										<label>State</label>
										<select class="form-control" name="state_id" id="state">
											<option>Select State</option>
											@if(!empty($states))
												@foreach($states as $key=>$value)
													<option value="{{ $value->id }}" @if($grampanchayat->state_id == $value->id) selected @endif>{{ $value->state_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-3">
										<label>District</label>
										<select class="form-control" name="district_id" id="district">
											<option>---</option>
										</select>
									</div>
									<div class="col-md-3">
										<label>Tehsil</label>
										<select class="form-control" name="tahsil_id" id="tehsil">
											<option>---</option>
										</select>
									</div>
									<div class="col-md-3">
										<label>Grampanchayat</label>
										<input type="text" value="{{$grampanchayat->panchayat_name}}" class="form-control" placeholder="Grampanchayat Name" name="panchayat_name" id="panchayat_name">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('admingrampanchayats') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('#state').trigger('change');
			$('#district').val({{ $grampanchayat->district_id }});
			$('#district').trigger('change');
			$('#tehsil').val({{ $grampanchayat->tahsil_id }});
			
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
		
		$('#state').change(function(){
			var value = $(this).val();
			$("#district option,#tehsil option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('getdistrict')}}",
					data:{'state_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#district").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#district").append(
								$("<option></option>").text(item.district_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
		
		$('#district').change(function(){
			var value = $(this).val();
			$("#tehsil option,#grampanchayat option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('gettehsil')}}",
					data:{'district_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#tehsil").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#tehsil").append(
								$("<option></option>").text(item.tehsil_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
    </script>
@endsection    
