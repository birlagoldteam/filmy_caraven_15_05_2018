<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>K Sera Sera - Box Office | Filmy Caravan | @yield('page_title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="K Sera Sera - Box Office | Filmy Caravan |" name="description" />
        <meta content="" name="author" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/layouts/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('public/admin/assets/layouts/css/themes/blue.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{URL::asset('public/admin/assets/layouts/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{URL::asset('public/admin/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
		<link rel="shortcut icon" href="{{URL::asset('public/admin/favicon.ico')}}" />
	</head>
	@yield('page_level_style_top')
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        @include('admin.includes.header')
        <div class="clearfix"> </div>
        <div class="page-container">
			@include('admin.includes.sidebar')
            <div class="page-content-wrapper">
                @yield('content')
            </div>
            @include('admin.includes.quicksidebar')
        </div>
		@include('admin.includes.footer')
		{{--@include('admin.includes.quicknav')--}}
		<script>
			$(document).ready(function() {
				@if(Session::has('message'))
					toastr.{{ Session::get('alert-type') }}("{{ Session::get('message') }}");
				@endif
			});
			
			function goBack() {
				window.history.back()
			}
		</script>
<!--
		<script src="{{URL::asset('public/admin/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
-->
		<script src="{{URL::asset('public/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/morris/morris.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/morris/raphael-min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/horizontal-timeline/horizontal-timeline.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/flot/jquery.flot.categories.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/pages/scripts/dashboard.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/layouts/scripts/layout.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/layouts/scripts/demo.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
		<script src="{{URL::asset('public/admin/assets/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>
		
		<!-- CDN for Toastr Notification -->	
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		<script type="text/javascript" src="//unpkg.com/sweetalert2@7.19.3/dist/sweetalert2.all.js"></script>
		@yield('page_level_script_bottom')
    </body>
</html>
