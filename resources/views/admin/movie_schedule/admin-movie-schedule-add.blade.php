@extends('admin.layouts.app')
@section('page_title')
    States
@endsection
@section('content')
	<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-datepicker/bootstrap-datepicker3.css')}}" />
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Add Movie Schedule</span>
                </li>
            </ul>
        </div>
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Add Movie Schedule </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="movieScheduleForm" name="movieScheduleForm" action="{{ route('admin-update-movie-schedule') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" value="" name="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-4">
										<label>State</label>
										<select class="form-control" name="state_id" id="state">
											<option value="">---</option>
											@if(!empty($states))
												@foreach($states as $key=>$state)
													<option value="{{ $state->id }}">{{ $state->state_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-4">
										<label>District</label>
										<select class="form-control" name="district_id" id="district">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Tehsil</label>
										<select class="form-control mb-md" name="tehsil_id" id="tehsil">
											<option value="">---</option>
										</select>
									</div>
								</div><br />
								<div class="row">
									<div class="col-md-4">
										<label>Grampanchayat</label>
										<select class="form-control mb-md" name="grampanchayat_id" id="grampanchayat">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Movie</label>
										<select class="form-control" name="movie_id" id="movie">
											<option value="">---</option>
											@if(!empty($movies))
												@foreach($movies as $key=>$value)
													<option value="{{ $value->id }}">{{ $value->movie_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-4">
										<label>Movie Date</label>
										<input type="date" name="movie_date" id="movie_date" class="form-control">
									</div>
								</div><br />
								<div class="row">
									<div class="col-md-4">
										<label>Movie Time</label>
										<input type="time" name="movie_time" class="form-control">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('admindistricts') }}" class="btn default cancel">Cancel</a>
								<input type="submit"  value="Submit" class="btn green" data-loading-text="Loading...">
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{URL::asset('public/admin/js/validation/jquery.validate.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/form-validation.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/app.js')}}"></script>
	<script src="{{URL::asset('public/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script>
		$(document).ready(function() {
			App.init();
			FormValidation.init();
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
		
		function formsubmit(form){
			form.submit();
		}
		
		$('#state').change(function(){
			var value = $(this).val();
			$("#district option,#tehsil option,#grampanchayat option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('getdistrict')}}",
					data:{'state_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#district").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#district").append(
								$("<option></option>").text(item.district_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
		
		$('#district').change(function(){
			var value = $(this).val();
			$("#tehsil option,#grampanchayat option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('gettehsil')}}",
					data:{'district_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#tehsil").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#tehsil").append(
								$("<option></option>").text(item.tehsil_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
		
		$('#tehsil').change(function(){
			var value = $(this).val();
			$("#grampanchayat option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('getgrampanchayat')}}",
					data:{'tehsil_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#grampanchayat").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#grampanchayat").append(
								$("<option></option>").text(item.panchayat_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
    </script>
@endsection    
