@extends('admin.layouts.app')
@section('page_title')
    Movie Schedule Attendances
@endsection
@section('content')
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Movie Schedule Attendances</span>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light tasks-widget ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Movie Schedule Attendances</span>
                            <span class="caption-helper"></span>
                            
                        </div>
                        <div class="actions">
							<button class="btn blue btn-outline" onclick="goBack()"><i class="fa fa-arrow-left"></i>&nbsp;Back</button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="task-content">
                            <div class="scroller" data-always-visible="1" data-rail-visible1="1">
								<form class="form-inline" role="form" action="{{ URL::route('movie-schedule-attendances',$attendances->id) }}" name="search" id="search">
									<div class="form-group">
										<label for="time">Time Filter</label>
										<select class="form-control" name="misscall_time" id="misscall_time">
											<option value="">---</option>
											<option value="<8PM"><?php echo "< 8PM"; ?></option>
											<option value="8 - 8.30 Pm"><?php echo "8 PM - 8.30 PM "; ?></option>
											<option value=">8:30 Pm"><?php echo "> 8:30 PM"; ?></option>
										</select>
									</div>
								</form><hr />
								<div class="row">
									<div class="col-md-3">
										<strong>Grampanchayat</strong> - {{ $attendances->grampanchayats->panchayat_name }}
									</div>
									<div class="col-md-3">
										<strong>Date</strong> - {{ date('dS M Y',strtotime($attendances->movie_date)) }}
									</div>
									<div class="col-md-3">
										<strong>Movie</strong> - {{ $attendances->movies->movie_name }}
									</div>
									<div class="col-md-3">
										<strong>Total</strong> - <span id="total_members"></span>
									</div>
								</div><hr />
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Sr. No</th>
												<th>Mobile No</th>
												<th>Date</th>
												<th>Time</th>
											</tr>
										</thead>
										<tbody>
											@if(!empty($attendances->attendances->all()))
												@foreach($attendances->attendances as $key=>$value)
													
													<?php
														$class="";
														$database  = strtotime(date('H:i', strtotime($value->created_at .'+330 minutes')));
														$time1 = strtotime(date('H:i',strtotime($value->schedule_date .'19:59:00')));
														$time2 = strtotime(date('H:i',strtotime($value->schedule_date .'20:00:00')));
														$time3 = strtotime(date('H:i',strtotime($value->schedule_date .'20:30:00')));
														if($database<$time1){
															$class = "time_first_option";
														}elseif($database>=$time2 && $database<=$time3){
															$class = "time_second_option";
														}elseif($database>$time3){
															$class = "time_third_option";
														}
													?>
												<tr class="all_time {{ $class }}">
													<td>{{$value->serial_no}}</td>
													<td>{{$value->mobile}}</td>
													<td>{{ date('d-m-Y',strtotime($value->created_at)) }}</td>
													<td>{{date('h:i A', strtotime($value->created_at .'+330 minutes'))}}</td>
<!--
													<td>{{ date('g:i A',strtotime($value->created_at)) }}</td>
-->
												</tr>
												@endforeach
											@else
												<tr><td colspan="3" align="center"><strong>No data Found !!</strong></td></tr>
											@endif
										</tbody>
									</table>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
		$(document).ready(function() {
			total_members('all_time');
			$('#misscall_time').change(function() {
				if($(this).val() == '<8PM') {
					$('.all_time').hide();
					$('.time_first_option').show();
					total_members('time_first_option');
				} else if($(this).val() == '8 - 8.30 Pm') {
					$('.all_time').hide();
					$('.time_second_option').show();
					total_members('time_second_option');
				} else if($(this).val() == '>8:30 Pm') {
					$('.all_time').hide();
					$('.time_third_option').show();
					total_members('time_third_option');
				} else {
					$('.all_time').show();
					total_members('all_time');
				}
			});
		});
		
		function total_members(i) {
			var numItems = $('.'+i).length;
			$('span#total_members').text(numItems);
		}
    </script>
@endsection    
