@extends('admin.layouts.app')
@section('page_title')
    States
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Edit Movie Schedule</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Edit Movie Schedule</div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="movieScheduleForm" name="movieScheduleForm" action="{{ route('admin-update-movie-schedule') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" value="{{$movieSchedules->id}}" name="id" name="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-4">
										<label>State</label>
										<select class="form-control" name="state_id" id="state">
											<option>Select State</option>
											@if(!empty($states))
												@foreach($states as $key=>$value)
													<option value="{{ $value->id }}" @if($movieSchedules->state_id == $value->id) selected @endif>{{ $value->state_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-4">
										<label>District</label>
										<select class="form-control" name="district_id" id="district">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Tehsil</label>
										<select class="form-control mb-md" name="tehsil_id" id="tehsil">
											<option value="">---</option>
										</select>
									</div>
								</div><br />
								<div class="row">
									<div class="col-md-4">
										<label>Grampanchayat</label>
										<select class="form-control mb-md" name="grampanchayat_id" id="grampanchayat">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Movie</label>
										<select class="form-control" name="movie_id" id="movie">
											<option value="">---</option>
											@if(!empty($movies))
												@foreach($movies as $key=>$value)
													<option value="{{ $value->id }}" @if($movieSchedules->movie_id == $value->id) selected @endif>{{ $value->movie_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-4">
										<label>Movie Date</label>
										<input type="date" name="movie_date" id="movie_date" value="{{$movieSchedules->movie_date}}" class="form-control">
									</div>
								</div><br />
								<div class="row">
									<div class="col-md-4">
										<label>Movie Time</label>
										<input type="time" name="movie_time" value="{{$movieSchedules->movie_time}}" class="form-control">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('adminmovieschedules') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			//App.init();
			//FormValidation.init();
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
			$('#state').trigger('change');
			$('#district').val('{{$movieSchedules->district_id}}');
			$('#district').trigger('change');
			$('#tehsil').val('{{$movieSchedules->tehsil_id}}');
			$('#tehsil').trigger('change');
			$('#grampanchayat').val('{{$movieSchedules->grampanchayat_id}}');
		});
		
		$('#state').change(function(){
			var value = $(this).val();
			$("#district option,#tehsil option,#grampanchayat option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('getdistrict')}}",
					data:{'state_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#district").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#district").append(
								$("<option></option>").text(item.district_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
		
		$('#district').change(function(){
			var value = $(this).val();
			$("#tehsil option,#grampanchayat option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('gettehsil')}}",
					data:{'district_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#tehsil").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#tehsil").append(
								$("<option></option>").text(item.tehsil_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
		
		$('#tehsil').change(function(){
			var value = $(this).val();
			$("#grampanchayat option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('getgrampanchayat')}}",
					data:{'tehsil_id':value},
					type: 'get',
					cache: false,
					async:false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#grampanchayat").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#grampanchayat").append(
								$("<option></option>").text(item.panchayat_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
    </script>
@endsection    
