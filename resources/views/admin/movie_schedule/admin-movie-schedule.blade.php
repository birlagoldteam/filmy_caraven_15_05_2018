@extends('admin.layouts.app')
@section('page_title')
    districts
@endsection
@section('content')
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Movie Schedule</span>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light tasks-widget ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Movie Schedule</span>
                            <span class="caption-helper"></span>
                            
                        </div>
                        <div class="actions">
							<a href="{{ URL::route('movie-schedule-edit') }}" type="button" class="btn blue btn-outline"><i class="fa fa-plus"></i>&nbsp;Add Movie Schedule</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="task-content">
                            <div class="scroller" data-always-visible="1" data-rail-visible1="1">
								<form class="form-inline" role="form" action="{{ route('movie-schedule-search') }}" name="search" id="search">
									<div class="form-group">
										<label for="movie_from_date">From Date</label>
										<input type="date" name="movie_from_date" id="movie_from_date" class="form-control">
										<label for="movie_to_date">To Date</label>
										<input type="date" name="movie_to_date" id="movie_to_date" class="form-control">
									</div>
									<button type="submit" class="btn btn-default">Search</button>
									<a href="{{ route('adminmovieschedules') }}" class="btn btn-primary" >Clear</a>
									<button type="button" class="btn btn-success excel">Export Excel</button
								</form>
<!--
								<form class="form-inline" role="form" action="{{ route('movie-schedule-search') }}" name="search" id="search">
									<div class="row">
										<div class="col-md-3 form-group">
											<label>Search Date</label> 
											<input type="date" name="movie_date" id="movie_date" class="form-control">
										</div>
										<div class="form-group col-md-3">
											
										</div>
									</div>                                                  
								</form>
-->
								<hr />
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Date/Time</th>
												<th>Movie</th>
												<th>Grampanchayat</th>
												<th>Tehsil</th>
												<th>District</th>
												<th>State</th>
												<th>&nbsp;</th>
											</tr>
										</thead>
										<tbody>
											@if(!empty($movieSchedules))
											@foreach($movieSchedules as $key=>$value)
											<tr>
												<td>
													{{ date('d-m-Y',strtotime($value->movie_date)) }}
													<span class="label label-success"> {{ date("g:i a", strtotime($value->movie_time)) }} </span>&nbsp;
													<span class="badge badge-danger tooltips" data-container="body" data-placement="top" data-original-title="Total Misscalls">{{ $value->total_members }}</span>
												</td>
												<td>{{$value->movie_name}}</td>
												<td>{{$value->panchayat_name}}</td>
												<td>{{$value->tehsil_name}}</td>
												<td>{{$value->district_name}}</td>
												<td>{{$value->state_name}}</td>
												<td align="right">
													<a href="{{ URL::route('movie-schedule-attendances',$value->id) }}" class="btn btn-xs yellow btn-outline"><i class="fa fa-user"></i></a>
													<a href="{{ URL::route('movie-schedule-edit',$value->id) }}" class="btn btn-xs green btn-outline"><i class="fa fa-edit"></i></a>
													<a href="{{ URL::route('movie-schedule-delete',$value->id) }}" class="btn btn-xs red btn-outline delete" rel="{{date('d-m-Y',strtotime($value->movie_date))}}"><i class="fa fa-trash"></i></a>
												</td>
											</tr>
											@endforeach
											@endif
										</tbody>
									</table>
								</div>
                            </div>
                        </div>
                        <div class="task-footer">
							
                            <div class="btn-arrow-link pull-right">
                                {!! $movieSchedules->links(); !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('.delete').click(function() {
				return confirm('Are you sure to delete Movie Schedule of '+$(this).attr('rel')+'?');
			});
			
			$('.excel').click(function(){
				window.location.href="{{ route('movie-schedule-report-export') }}?"+$('#search').serialize();
			});
		});
    </script>
@endsection    
