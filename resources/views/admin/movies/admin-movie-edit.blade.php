@extends('admin.layouts.app')
@section('page_title')
    Movies
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Edit Movie</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Edit Movie {{$movie->movie_name}} </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form id="movieForm" name="movieForm" class="form-horizontal" action="{{ route('admin-update-movie') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" value="{{$movie->id}}" name="id">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<input type="text" value="{{$movie->movie_name}}" class="form-control" placeholder="Movie Name" name="movie_name" id="movie_name">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<!--<label>Active/Inactive</label>-->
										<div class="mt-checkbox-list">
											<label class="mt-checkbox mt-checkbox-outline"> Is Active
												<input type="checkbox" value="{{$movie->active}}" name="active" @if($movie->active == '1') checked @endif>
												<span></span>
											</label>
										</div>                                            
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('adminmovies') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{URL::asset('public/admin/js/validation/jquery.validate.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/form-validation.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/app.js')}}"></script>
    <script>
		$(document).ready(function() {
			App.init();
			FormValidation.init();
			
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
			
			$("input[type='checkbox']").on('change', function(){
				$(this).val(this.checked ? "1" : "0");
			});
		});
		
		function formsubmit(form){
			form.submit();
		}
    </script>
    <!-- END CONTENT BODY -->
@endsection    
