@extends('admin.layouts.app')
@section('page_title')
    Movies
@endsection
@section('content')
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Movies</span>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light tasks-widget ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Movies</span>
                            <span class="caption-helper"></span>
                        </div>
                        <div class="actions">
							<a href="{{ URL::route('movie-edit') }}" type="button" class="btn blue btn-outline"><i class="fa fa-plus"></i>&nbsp;Add Movie</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="task-content">
                            <div class="scroller" style="height: 312px;" data-always-visible="1" data-rail-visible1="1">
                                <ul class="task-list">
                                    @if(!empty($movies))
                                    @foreach($movies as $key=>$value)
                                    <li>
                                        <div class="task-title">
                                            <span class="task-title-sp"> {{$value->movie_name}} </span>
                                            @if($value->active)
                                            <span class="label label-sm label-success">Active</span>
                                             @else
                                             <span class="label label-sm label-danger">Inactive</span>
                                             @endif
                                        </div>
                                        <div class="task-config">
											<a href="{{ URL::route('movie-edit',$value->id) }}" class="btn btn-xs green btn-outline"><i class="fa fa-edit"></i></a>
											<a href="{{ URL::route('movie-delete',$value->id) }}" class="btn btn-xs red btn-outline delete" rel="{{$value->movie_name}}"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="task-footer">
							
                            <div class="btn-arrow-link pull-right">
                                {!! $movies->links(); !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('.delete').click(function() {
				return confirm('Are you sure to delete movie '+$(this).attr('rel')+'?');
			});
		});
    </script>
@endsection    
