@extends('admin.layouts.app')
@section('page_title')
    States
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Add State</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <!--<div class="portlet light tasks-widget ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Add State</span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="task-content">
                            <div class="scroller" style="height: 312px;" data-always-visible="1" data-rail-visible1="1">
								<div class="form-body">
									<div class="form-group">
										<label>State Name</label>
										<div class="input-group">
											
											<input type="text" class="form-control" placeholder="State"> </div>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="task-footer">
                            <div class="btn-arrow-link pull-right">
                                <a href="javascript:;">See All Records</a>
                                <i class="icon-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Add State </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
							<!--<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
							<a href="" class="reload" data-original-title="" title=""> </a>
							<a href="" class="remove" data-original-title="" title=""> </a>-->
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" action="{{ route('admin-update-state') }}" method="POST" role="form">
							{{ csrf_field() }}
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-12">
										<input type="hidden" value="" name="id">
										<input type="text" class="form-control" placeholder="State Name" name="state_name" id="state_name">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('adminstates') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
    </script>
    <!-- END CONTENT BODY -->
@endsection    
