@extends('admin.layouts.app')
@section('page_title')
    Categories
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Edit Sub Category</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Edit Sub Category {{$subCategory->sub_category_name}} </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="subCategoryForm" name="subCategoryForm" action="{{ route('admin-update-sub-category') }}" method="POST" role="form">
							{{ csrf_field() }}
							<div class="form-body">
								<div class="row">
									<div class="col-md-6">
										<input type="hidden" value="{{$subCategory->id}}" name="id" name="id">
										<select class="form-control" name="category_id" id="category_id">
											<option>Select Category</option>
											@if(!empty($categories))
												@foreach($categories as $key=>$value)
													<option value="{{ $value->id }}" @if($subCategory->category_id == $value->id) selected @endif>{{ $value->category_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-6">
										<input type="text" value="{{$subCategory->sub_category_name}}" class="form-control" placeholder="Sub Category Name" name="sub_category_name" id="sub_category_name">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('adminsubcategories') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
    </script>
@endsection    
