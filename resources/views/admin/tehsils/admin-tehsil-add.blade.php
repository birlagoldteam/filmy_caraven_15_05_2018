@extends('admin.layouts.app')
@section('page_title')
    States
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Add Tehsil</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Add Tehsil </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
							<!--<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
							<a href="" class="reload" data-original-title="" title=""> </a>
							<a href="" class="remove" data-original-title="" title=""> </a>-->
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="tehsilForm" name="tehsilForm" action="{{ route('admin-update-tehsil') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" value="" name="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-4">
										<label>State</label>
										<select class="form-control" name="state_id" id="state">
											<option>Select State</option>
											@if(!empty($states))
												@foreach($states as $key=>$value)
													<option value="{{ $value->id }}">{{ $value->state_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-4">
										<label>District</label>
										<select class="form-control" name="district_id" id="district">
											<option>---</option>
										</select>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Tehsil Name</label>
											<input type="text" class="form-control" placeholder="Tehsil Name" name="tehsil_name" id="tehsil_name">
										</div>
									</div>
								</div>
								<div class="row">.
									<div class="col-md-4">
										<label>Mobile</label>
										<input type="text" class="form-control" placeholder="Mobile No" name="mobile_no" id="mobile_no">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('admintehsils') }}" class="btn default cancel">Cancel</a>
								<input type="submit"  value="Submit" class="btn green" data-loading-text="Loading...">
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{URL::asset('public/admin/js/validation/jquery.validate.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/form-validation.js')}}"></script>
	<script src="{{URL::asset('public/admin/js/validation/app.js')}}"></script>
    <script>
		$(document).ready(function() {
			App.init();
			FormValidation.init();
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
		
		function formsubmit(form){
			//$('#loading').show();
			form.submit();
		}
		
		$('#state').change(function(){
			var value = $(this).val();
			$("#district option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('getdistrict')}}",
					data:{'state_id':value},
					type: 'get',
					cache: false,
					clearForm: false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#district").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#district").append(
								$("<option></option>").text(item.district_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
    </script>
    <!-- END CONTENT BODY -->
@endsection    
