@extends('admin.layouts.app')
@section('page_title')
    States
@endsection
@section('content')
 <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{ route('admindashboard') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Edit Tehsil</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
       
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i> Edit Tehsil {{$tehsil->tehsil_name}} </div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
							<!--<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
							<a href="" class="reload" data-original-title="" title=""> </a>
							<a href="" class="remove" data-original-title="" title=""> </a>-->
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" id="tehsilForm" name="tehsilForm" action="{{ route('admin-update-tehsil') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" value="{{$tehsil->id}}" name="id" name="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-4">
										<select class="form-control" name="state_id" id="state">
											<option>Select State</option>
											@if(!empty($states))
												@foreach($states as $key=>$value)
													<option value="{{ $value->id }}" @if($tehsil->state_id == $value->id) selected @endif>{{ $value->state_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-4">
										<select class="form-control" name="district_id" id="district">
											<option>---</option>
										</select>
									</div>
									<div class="col-md-4">
										<input type="text" value="{{$tehsil->tehsil_name}}" class="form-control" placeholder="Tehsil Name" name="tehsil_name" id="tehsil_name">
									</div>
								</div><br />
								<div class="row">.
									<div class="col-md-4">
										<label>Mobile</label>
										<input type="text" value="{{$tehsil->mobile_no}}" class="form-control" placeholder="Mobile No" name="mobile_no" id="mobile_no">
									</div>
								</div>
							</div>
							<div class="form-actions right">
								<a href="{{ route('admintehsils') }}" class="btn default cancel">Cancel</a>
								<button type="submit" class="btn green">Submit</button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script>
		$(document).ready(function() {
			$('#state').change();
			$('#district').val({{ $tehsil->district_id }});
			
			$('.cancel').click(function() {
				return confirm('Are you sure?');
			});
		});
		
		$('#state').on('change',function(){
			console.log('dfdf');
			var value = $('#state option:selected').val();
			$("#district option").remove();
			if(value!=""){
				$.ajax({
					url: "{{ route('getdistrict')}}",
					data:{'state_id':value},
					type: 'get',
					cache: false,
					clearForm: false,
					async:false,
					beforeSend:function(){  
						$('#loading').show();
					},
					success: function(response){
						$("#district").append($("<option></option>").text('---').val(''));
						$.each(response,function(index,item){
							$("#district").append(
								$("<option></option>").text(item.district_name).val(item.id)
							);
						});
					},
					error:function(){
						alert("Server is Busy!!");
					},
					complete:function(data){
						$('#loading').hide();
					}
				});
			}
		});
    </script>
@endsection    
