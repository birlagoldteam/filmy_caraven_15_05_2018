@extends('layouts.app')
@section('page_title')
    Ngo Login
@endsection
@section('content')
<section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Social Welfare Partners Login</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{ route('welcome') }}">Home</a></li>
                        <li class="active">Login</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
   <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <div class="featured-boxes">
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <div class="featured-box featured-box-primary align-left mt-xlg">
                                <div class="box-content">
                                    <h4 class="heading-primary text-uppercase mb-md">I'm a Returning Social Welfare Partners</h4>
                                    @if(Session::has('msg'))
                                        <div class="alert alert-danger">
                                            {{ Session::get('msg') }}
                                        </div>
                                    @endif
                                    <form action="{{ route('ngo-login-submit') }}" id="frmSignIn" method="post">
                                        {{ csrf_field() }}  
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                     <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"> </div>
                                                    <label>E-mail Address</label>
                                                    <input type="text" value="" class="form-control input-lg" name="email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <a class="pull-right" href="" data-toggle="modal" data-target="#forgotpassword">(Lost Password?)</a>
                                                    <label>Password</label>
                                                    <input type="password" value="" class="form-control input-lg" name="password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="submit" value="Login" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="forgotpassword" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="smallModalLabel">Forgot Password</h4>
                </div>
                <div class="modal-body">
                    <form id="demo-form" class="form-horizontal" novalidate="novalidate">
                        <div class="form-group mt-lg">
                            <div class="col-sm-2"><label>Email:</label></div>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" placeholder="Enter Your Email" name="otpffghf" id="otpffghf" required/>
                            </div>
                        </div>
                        <div class="form-group mt-lg" style="display:none;" id="showhide">
                            <div class="col-sm-2"><label>OTP:</label></div>
                            <div class="col-sm-7">
                                <input type="text" name="opt" class="form-control" placeholder="Enter OTP" id="opt" required/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="registeruser">Send OTP</button>
                    <button type="button" class="btn btn-default" id="verifyotp" style="display:none;">Verify</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="closemodal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/app.js')}}"></script>
    <script>
        jQuery(document).ready(function(){
            App.init();
            FormValidation.init();
        });

        $('#registeruser').click(function(){
            var value = $('#otpffghf').val();
            if(value==""){
                alert("Please Enter Email!!!");
                return false;
            }
            $.ajax({
                url: "{{ route('ngocheckemail')}}",
                data:{'email':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                    if(response['success']===false){
                        alert(response['error']);
                        return false;
                    }
                    $('#showhide,#verifyotp').show();
                    $('#registeruser').html('Resend Otp');
                    $('#otpffghf').prop('readonly',true);
                    alert("Otp Send To Email!!!");
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
        });
    
        $('#verifyotp').click(function(){
            var otp = $('#opt').val();
            var email = $('#otpffghf').val();
            if(otp==""){
                alert("Please Enter Otp!!!");
                return false;
            }
            $.ajax({
                url: "{{ route('ngocheckemailotp')}}",
                data:{'email':email,'otp':otp},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                    if(response['success']===false){
                        alert(response['error']);
                        return false;
                    }
                    window.location.href="{{ route('forgotpassword') }}";
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
        });

        $('#closemodal').click(function(){
            $('#opt').val('');
            $('#showhide,#verifyotp').hide();
            $('#registeruser').html('Send Otp');
            $('#otpffghf').prop('readonly',false).val('');
        });
    </script>
@endsection

