@extends('layout.master')
@section('page_title')
    Email Template
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Email Template</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Email Template</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
			<header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-4">Email Template</h2>
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success col-md-5" style="padding:7px;margin-bottom: -8px;">
                        	{{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if($errors->any())
					    <div class="alert alert-danger col-md-5 message" style="padding:7px;margin-bottom: -8px;">
					        @foreach($errors->all() as $error)
					            <p>{{ $error }}</p>
					        @endforeach
					    </div>
                    @endif
                </div>
            </header>
			<div class="panel-body">
				<form action="{{ route('addemail-template') }}" class="form-horizontal" id="validateemailtemplate" method="post" enctype="multipart/form-data">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Title
							<span class="required">
								*
							</span>
							</label>
							<div class="col-md-4">
								<input type="text" name="title" data-required="1" class="form-control" placeholder="Please Enter Title" value="{{Request::old('title')}}" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Subject
							<span class="required">
									*
								</span>
							</label>
							<div class="col-md-4">
								<input type="text" name="subject" data-required="1" class="form-control" placeholder="Please Enter Order" value="{{Request::old('subject')}}" />
							</div>
						</div>
						<div class="form-group last">
							<label class="control-label col-md-3">Content
							<span class="required">
								*
							</span>
							</label>
							<div class="col-md-9">
								<textarea class="ckeditor form-control" name="content" id="editor1" rows="6" data-error-container="#editor2_error"></textarea>
								<div id="editor2_error">
								</div>
							</div>
						</div>
						<div class ="row from-group">
							<div class="control-label col-md-3">
							 	<input class="pull-right btn default" type="Button" value="Add More" id="addmore">
							</div>
							<div class="col-md-9" id="fileadd">
								<input type="file" name="attach[]"> <br>
							</div>
						</div>
						<div class="form-actions fluid">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn green">Submit</button>
								<input type="reset" class="btn default" value="Cancel">
								<input type="hidden" name="_token" value="{{ Session::token() }}">
							</div>
						</div>
					</div>
				</form>
			</div>		
		</section>
	</section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/ckeditor/ckeditor.js')}}"></script>
   	<script>
        jQuery(document).ready(function () {
            $('#loading').hide(); 
        });
		$('#addmore').click(function(){
			$('#fileadd').append('<input type="file" name="attach[]"><br>');
		});

		$('#inserttxtmail').click(function(){
			var append=$('#placeholdermail option:selected').val();
			CKEDITOR.instances['editor1'].insertText('['+append+']');
		});
	</script>
@endsection