
<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 37, 'stickySetTop': '-37px', 'stickyChangeLogo': false}">
    <div class="header-body background-color-custom-primary pt-none pb-none">
        <div class="header-top header-top img-responsive header-top-style-3 header-top-custom background-color-custom-secondary m-none" style='background-image: url({{ URL::asset("public/img/demos/real-estate/top-header.png")}});'>
            <div class="container">
                <nav class="header-nav-top pull-left">
                    <ul class="nav nav-pills">
                        <!-- <li class="hidden-xs">
                            <span class="ws-nowrap"><i class="icon-location-pin icons"></i> 315 Andheri (W), Mumbai</span>
                        </li>
                        <li>
                            <span class="ws-nowrap"><i class="icon-call-out icons"></i> (800) 123-4567</span>
                        </li> -->
                        <li class="hidden-xs">
                            <span class="ws-nowrap"><i class="icon-envelope-open icons"></i> <a class="text-decoration-none" href="info@kssboxoffice.com">info@kssboxoffice.com</a></span>
                        </li>
                    </ul>
                </nav>
                <nav class="header-nav-top langs pull-right mr-none">
                    @if(!(Auth::guard('web')->check() || Auth::guard('ngo')->check()))
                        <ul class="nav nav-pills" style="color: #e94b1c;">
                            <li>
                                <a href="#" class="dropdown-menu-toggle" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Log In
                                    <i class="fa fa-sort-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
                                    <li>
                                        <a href="{{route('login')}}"><i class="fa fa-user"></i> User</a>
                                    </li>
                                    <li>
                                        <a href="{{route('ngo-login')}}"><i class="fa fa-user"></i> NGO</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="blog">
                               
                                <a href="#" class="dropdown-menu-toggle" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Register
                                    <i class="fa fa-sort-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
                                    <li>
                                        <a href="{{route('memberregister')}}"><i class="fa fa-user"></i> User</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('ngo-registration') }}"><i class="fa fa-user"></i> NGO</a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
                    @else
                    <ul class="nav nav-pills" style="color: #e94b1c;">
                        <li class="blog">
                            <a href="#" class="dropdown-menu-toggle" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                {{ Auth::guard('web')->check()==true ? Auth::user()->name : Auth::guard('ngo')->user()->name  }}
                                <i class="fa fa-sort-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
								@if(Auth::guard('web')->check()==true)
                                <li><a href="{{route('membershipregistration')}}"><i class="fa fa-user"></i> My Account</a></li>
                                @else
                                <li><a href="{{route('addngodetails')}}"><i class="fa fa-user"></i> My Account</a></li>
                                @endif
                                <li>
                                    <a href="{{ route('logout') }}" 
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="fa fa-sign-out"></i>
                                        {{ __('Log Out') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                        <!-- <div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </div> -->
                    @endif
                </nav>
            </div>
        </div>
        <div class="header-container container custom-position-initial">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="{{ route('welcome')}}">
                            <img alt="K Sera Sera" width="170" height="55" src=" {{URL::asset('public/img/demos/real-estate/logo1.png')}}">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-nav">
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                <i class="fa fa-bars"></i>
                            </button>
                            <?php $active = request()->route()->getName();?>
                            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse m-none">
                                <nav>
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li  @if($active=='welcome') class="dropdown-full-color dropdown-quaternary active"  @else class="dropdown-full-color dropdown-quaternary"  @endif>
                                            <a href="{{route('welcome')}}">
                                                Home
                                            </a>
                                        </li>
                                        <li  @if(in_array($active,['whatwedo','managementteam','about'])) class="dropdown dropdown-full-color dropdown-quaternary active"  @else class="dropdown dropdown-full-color dropdown-quaternary"  @endif>
                                            <a class="dropdown-toggle" href="#">
                                                About
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li style=""><a href="{{route('about')}}">Company Profile </a></li>
                                                <!-- <li><a href="kss-box-office-who-we-are.php">Who We Are</a></li> -->
                                                <li><a href="{{route('whatwedo')}}">What We do</a></li>
                                                <li><a href="{{route('managementteam')}}">Management Team</a></li>
                                            </ul>
                                        </li>
                                        <li @if(in_array($active,['production','distribution','filmexhibition'])) class="dropdown dropdown-full-color dropdown-quaternary active"  @else class="dropdown dropdown-full-color dropdown-quaternary"  @endif>
                                            <a class="dropdown-toggle">
                                                Business Vertical
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{route('production')}}">Production</a></li>
                                                <li><a href="{{route('distribution')}}">Distribution</a></li>
                                                <li><a href="{{route('filmexhibition')}}">Film Exhibition</a></li>
                                            </ul>
                                        </li>
                                        <li @if($active=="filmycaravan") class="dropdown-full-color dropdown-quaternary active"  @else class="dropdown-full-color dropdown-quaternary"  @endif>
                                            <a href="{{route('filmycaravan')}}">
                                                Filmy Caravan
                                            </a>
                                        </li>
                                        <li @if($active=="opportunity") class="dropdown-full-color dropdown-quaternary active"  @else class="dropdown-full-color dropdown-quaternary"  @endif>
                                            <a href="{{route('opportunity')}}">
                                                Opportunity
                                            </a>
                                        </li>
                                        <li @if($active=="contact") class="dropdown-full-color dropdown-quaternary active"  @else class="dropdown-full-color dropdown-quaternary"  @endif>
                                            <a href="{{route('contact')}}">
                                                Contact Us
                                            </a>
                                        </li>
                                        <li class="dropdown dropdown-full-color dropdown-quaternary dropdown-mega" id="headerSearchProperties">
                                            <a class="dropdown-toggle" href="#">
                                                Movie Schedule <i class="fa fa-search"></i>
                                            </a>
                                            <ul class="dropdown-menu custom-fullwidth-dropdown-menu">
                                                <li>
                                                    <div class="dropdown-mega-content">
                                                        <form id="propertiesFormHeader" action="{{ route('getfilm') }}" method="POST">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <div class="container p-none">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase font-size-sm" name="stateheader" data-msg-required="This field is required." id="stateheader" required="">
                                                                                <option value="">State</option>
                                                                                @foreach($statedata as $value)
                                                                                   <option value="{{ $value->id }}">{{ $value->state_name }}</option> 
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase font-size-sm" name="propertiesLocation" data-msg-required="This field is required." id="cityheader" required="">
                                                                                <option value="">District</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase font-size-sm" name="propertiesMinBeds" data-msg-required="This field is required." id="tehsilheader" required="">
                                                                                <option value="">Tehsil</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase font-size-sm" name="propertiesMinPrice" data-msg-required="This field is required." id="grampanchayatheader" required="">
                                                                                <option value="">Gram Panchayat</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div class="col-md-2">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase font-size-sm" name="propertiesMaxPrice" data-msg-required="This field is required." id="propertiesMaxPrice" required="">
                                                                                <option value="">Village</option>
                                                                                <option value="150000">$150,000</option>
                                                                                <option value="200000">$200,000</option>
                                                                                <option value="250000">$250,000</option>
                                                                                <option value="300000">$300,000</option>
                                                                                <option value="350000">$350,000</option>
                                                                                <option value="400000">$400,000</option>
                                                                                <option value="450000">$450,000</option>
                                                                                <option value="500000">$500,000</option>
                                                                                <option value="550000">$550,000</option>
                                                                                <option value="600000">$600,000</option>
                                                                                <option value="650000">$650,000</option>
                                                                                <option value="700000">$700,000</option>
                                                                                <option value="750000">$750,000</option>
                                                                                <option value="800000">$800,000</option>
                                                                                <option value="850000">$850,000</option>
                                                                                <option value="900000">$900,000</option>
                                                                                <option value="950000">$950,000</option>
                                                                                <option value="1000000">$1,000,000</option>
                                                                                <option value="1250000">$1,250,000</option>
                                                                                <option value="1500000">$1,500,000</option>
                                                                                <option value="1750000">$1,750,000</option>
                                                                                <option value="2000000">$2,000,000</option>
                                                                                <option value="2250000">$2,250,000</option>
                                                                                <option value="2500000">$2,500,000</option>
                                                                                <option value="2750000">$2,750,000</option>
                                                                                <option value="3000000">$3,000,000</option>
                                                                                <option value="3250000">$3,250,000</option>
                                                                                <option value="3500000">$3,500,000</option>
                                                                                <option value="3750000">$3,750,000</option>
                                                                                <option value="4000000">$4,000,000</option>
                                                                                <option value="4250000">$4,250,000</option>
                                                                                <option value="4500000">$4,500,000</option>
                                                                                <option value="4750000">$4,750,000</option>
                                                                                <option value="5000000">$5,000,000</option>
                                                                                <option value="6000000">$6,000,000</option>
                                                                                <option value="7000000">$7,000,000</option>
                                                                                <option value="8000000">$8,000,000</option>
                                                                                <option value="9000000">$9,000,000</option>
                                                                                <option value="10000000">$10,000,000</option>
                                                                            </select>
                                                                        </div>
                                                                    </div> -->
                                                                    <div class="col-md-2">
                                                                        <input type="submit" value="Search Now" class="btn btn-secondary btn-lg btn-block text-uppercase font-size-sm">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
