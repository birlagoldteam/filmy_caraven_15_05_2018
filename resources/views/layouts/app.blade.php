<!DOCTYPE html>
<html>
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	
		<title>K Sera Sera - Box Office | Filmy Caravan | @yield('page_title')</title>	
		<meta name="keywords" content="K Sera Sera, Box Office, Filmy Caravan" />
		<meta name="description" content="K Sera Sera - Box Office | Filmy Caravan">
		<meta name="author" content="Mr. Sukhsagar Kumawat">
		<link rel="shortcut icon" href="{{URL::asset('public/img/favicon.ico')}}" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link rel="stylesheet" href="{{URL::asset('public/css/font.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/bootstrap/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/animate/animate.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/magnific-popup/magnific-popup.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/css/theme.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/css/theme-elements.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/css/theme-blog.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/css/theme-shop.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/rs-plugin/css/settings.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/rs-plugin/css/layers.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/vendor/rs-plugin/css/navigation.css')}}">
		<link rel="stylesheet" href="{{URL::asset('public/css/skins/skin-real-estate.css')}}"> 
		<link rel="stylesheet" href="{{URL::asset('public/css/demos/demo-real-estate.css')}}">
		<link rel="stylesheet" href="{{ URL::asset('public/css/custom.css')}}">
		<script src="{{ URL::asset('public/vendor/modernizr/modernizr.min.js')}}"></script>
		@yield('page_level_style_top')
	</head>
	<body class="loading-overlay-showing" data-loading-overlay data-spy="scroll" data-target="#sidebar" data-offset="120">
		<div class="loading-overlay">
			<div class="bounce-loader">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
		<div class="body">
			@include('includes.header')
			<div role="main" class="main">
				@yield('content')
				@include('includes.footer')
			</div>
		</div>
		<script src="{{URL::asset('public/js/jquery.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/jquery-cookie/jquery-cookie.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/common/common.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/isotope/jquery.isotope.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/vide/vide.min.js')}}"></script>
		<script src="{{URL::asset('public/js/theme.js')}}"></script>
		<script src="{{URL::asset('public/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
		<script src="{{URL::asset('public/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
		<script src="{{URL::asset('public/js/custom.js')}}"></script>
		<script src="{{URL::asset('public/js/theme.init.js')}}"></script>
		@yield('page_level_script_bottom')
		<script type="text/javascript">
			$(document).ready(function(){

			});

			$('#stateheader').change(function(){
				var value = $(this).val();
				$("#cityheader option,#tehsilheader option:not(:first),#grampanchayatheader:not(:first) option").remove();
				if(value!=""){
					$.ajax({
			            url: "{{ route('getdistrict')}}",
			            data:{'state_id':value},
			            type: 'get',
			            cache: false,
			            clearForm: false,
			            beforeSend:function(){  
			                $('#loading').show();
			            },
			            success: function(response){
			            	$("#cityheader").append($("<option></option>").text('District').val(''));
			               	$.each(response,function(index,item){
						      	$("#cityheader").append(
						      		$("<option></option>").text(item.district_name).val(item.id)
						       	);
						    });
						},
			            error:function(){
			                alert("Server is Busy!!");
			            },
			            complete:function(data){
			                $('#loading').hide();
			            }
			        });
				}
			});

			$('#cityheader').change(function(){
				var value = $(this).val();
				$("#tehsilheader option:not(:first),#grampanchayatheader option:not(:first)").remove();
				if(value!=""){
					$.ajax({
			            url: "{{ route('gettehsil')}}",
			            data:{'district_id':value},
			            type: 'get',
			            cache: false,
			            clearForm: false,
			            beforeSend:function(){  
			                $('#loading').show();
			            },
			            success: function(response){
			            	$.each(response,function(index,item){
						      	$("#tehsilheader").append(
						      		$("<option></option>").text(item.tehsil_name).val(item.id)
						       	);
						    });
						},
			            error:function(){
			                alert("Server is Busy!!");
			            },
			            complete:function(data){
			                $('#loading').hide();
			            }
			        });
				}
			});

			$('#tehsilheader').change(function(){
				var value = $(this).val();
				$("#grampanchayatheader option:not(:first)").remove();
				if(value!=""){
					$.ajax({
			            url: "{{ route('getgrampanchayat')}}",
			            data:{'tehsil_id':value},
			            type: 'get',
			            cache: false,
			            clearForm: false,
			            beforeSend:function(){  
			                $('#loading').show();
			            },
			            success: function(response){
			            	$.each(response,function(index,item){
						      	$("#grampanchayatheader").append(
						      		$("<option></option>").text(item.panchayat_name).val(item.id)
						       	);
						    });
						},
			            error:function(){
			                alert("Server is Busy!!");
			            },
			            complete:function(data){
			                $('#loading').hide();
			            }
			        });
				}
			})

		</script>
	</body>
</html>
