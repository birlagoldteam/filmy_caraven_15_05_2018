@extends('layouts.app')
@section('page_title')
    Members
@endsection
@section('content')
   <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>My Account</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Dashboard</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="agent-item">
            <div class="row">
                <!-- <div class="col-md-2">
                    <img src="img/team/team-11.jpg" class="img-responsive" alt="">
                </div> -->
                <div class="col-md-6">
                    <h3>Sukhsagar Kumawat</h3>
                    <!-- <h6 class="mb-xs">ssss</h6> -->
                    <h5 class="mt-xs mb-xs">Click <i class="fa fa-hand-o-down"></i> for Membership Registration </h5>
                    <a class="btn btn-secondary btn-sm mt-md" href="#membership">Membership Registration</a>
                </div>
                <div class="col-md-4">
                    <ul class="list list-icons m-sm ml-xl">
                        <li>
                            <a href="mailto: mail@domain.com">
                                <i class="icon-envelope-open icons"></i> sukhsagar@domain.com
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-call-out icons"></i> (800) 123-4567
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-linkedin icons"></i> Lindekin
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-facebook icons"></i> Facebook
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div id="membership" class="heading heading-border heading-middle-border heading-middle-border-center">
            <h2>Filmy Caravan <strong>Membership Registration</strong></h2>
        </div>

        <!-- <div id="membership" class="row">
            <h2 align="center">Filmy Caravan Membership Registration</h2>
        </div> -->

        <div class="row">
            <div class="col-md-12">
                <div class="agent-item agent-item-detail">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="contactForm" action="php/contact-form.php" method="POST">
                                <input type="hidden" value="Contact Form" name="subject" id="subject">
                                
                                <div class="row">
                                    <h4>1. Personal Info.</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Name *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Email *</label>
                                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Mobile *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Gender *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Date of Birth *</label>
                                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Aadhar No. *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>PAN *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Marital Status *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Children *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Blod Group *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                               
                                
                                    <h5>Address</h5>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Country *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>State *</label>
                                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City/District *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">    
                                        <div class="col-md-4">
                                            <label>Tehsil *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Gram Panchayat *</label>
                                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Village *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">    
                                        <div class="col-md-4">
                                            <label>Stree/Area *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>House Name/Number *</label>
                                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Pincode *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                               
                                    <h5>Education</h5>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Country *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Language Known *</label>
                                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                                        </div>
                                    </div>
                                
                                    <h5>Employment</h5>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Country *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Other *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                               

                                    <h5>Health Profile</h5>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Handicapped *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Any Long-Term Diseases  *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>

                                    <h4>2. Family Info.</h4>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-condensed mb-none">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Age</th>
                                                            <th>Relationship</th>
                                                            <th>Employment</th>
                                                            <th>Handicapped</th>
                                                            <th>Any Long-Term Diseases</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right">
                                                                <label class="checkbox-inline">
                                                                    <input type="radio" id="inlineCheckbox1" value="option1"> Yes
                                                                </label>
                                                                <label class="checkbox-inline">
                                                                    <input type="radio" id="inlineCheckbox1" value="option1"> No
                                                                </label>
                                                            </td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                            <td class="text-right"><input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>
                                
                                    <h4>3. Income Info.</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Type of Work *</label>
                                            <select class="form-control mb-md">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Income *</label>
                                            <select class="form-control mb-md">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Vehicle *</label>
                                            <select class="form-control mb-md">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>House *</label>
                                            <select class="form-control mb-md">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Loan *</label>
                                            <select class="form-control mb-md">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Insurance *</label>
                                            <select class="form-control mb-md">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <h4>4. Social Welfare</h4>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Social Welfare Needs *</label></div>
                                            <div class="col-md-12">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> Education
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox2" value="option2"> Water & Sanitation
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> Healthcare
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox4" value="option4"> Environmental
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox5" value="option5"> Social & Economic Empowerment
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" id="inlineCheckbox6" value="option6"> Sports & Culture
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <!-- <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Message *</label>
                                            <textarea maxlength="5000" data-msg-required="Please enter your message." rows="5" class="form-control" name="message" id="message" required></textarea>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="submit" value="Submit" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection