@extends('layouts.app')
@section('page_title')
    Social Welfare Partner Registration
@endsection
@section('page_level_style_top')
<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
   <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Social Welfare Partner Updation</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Social Welfare Partner Updation</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
            
    <div class="container">
        <div id="membership" class="heading heading-border heading-middle-border heading-middle-border-center">
            <h2><strong>Social Welfare Partner Updation</strong></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="agent-item agent-item-detail">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="ngoRegistrationForm" name="ngoRegistrationForm" action="{{ route('update_ngo') }}" method="post" enctype="multipart/form-data"  files=true>
                                {{ csrf_field() }}
                                <input type="hidden" value="Contact Form" name="subject" id="subject">
                                
                                <div class="row">
                                    <h4>1. Registration Details</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Registration For </label>
                                            <select class="form-control mb-md" name="register_for" id="register_for">
                                                <option value="">---</option>
                                                @foreach(config('custome.ngoRegisterFor') as $key=>$option)
                                                    <option value="{{$key}}" <?php if ($ngo->register_for == $key) echo 'selected'; ?>>{{$option}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                           <label>Name</label>
                                            <input type="text" value="{{$ngo->name}}" data-msg-="Please enter your name." maxlength="100" class="form-control" name="name" id="name">
                                        </div>
                                        <div class="col-md-4">
                                           <label>Unique Id Of VO/NGo</label>
                                            <input type="text" value="{{$ngo->unique_id}}" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="unique_id" id="unique_id" 
                                            required>
                                        </div>
                                        
                                    </div>
                                  
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Registered With</label>
                                            <select class="form-control mb-md" name="registered_with" id="registered_with">
                                                <option value="">---</option>
                                                @foreach(config('custome.ngoRegisteredwith') as $key=>$option)
                                                    <option value="{{$key}}" <?php if ($ngo->registered_with == $key) echo 'selected'; ?>>{{$option}}</option>
                                                @endforeach
                                            </select>
                                           <!--  <label>Registered With</label>
                                            <input type="text" value="{{$ngo->registered_with}}" data-msg-="Please enter your name." maxlength="100" class="form-control" name="registered_with" id="registered_with"> -->
                                        </div>
                                        <div class="col-md-4 registered_with_other" style="display:none;">
                                            <label>Other</label>
                                            <input type="text" value="{{$ngo->registered_with_other}}" data-msg-="Please enter your name." maxlength="100" class="form-control" name="registered_with_other" id="registered_with_other">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Registration NO</label>
                                            <input type="text" value="{{$ngo->registration_no}}" data-msg-="Please enter your name." maxlength="100" class="form-control" name="registration_no" id="registration_no">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>State Of Registration</label>
                                            <select class="form-control mb-md" name="ngo_register_state" id="ngo_register_state">
                                                <option value="">---</option>
                                                @foreach($state as $value)
                                                    <option value="{{$value->id}}" <?php if ($ngo->ngo_register_state == $value->id) echo 'selected'; ?>>{{$value->state_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City Of Registration</label>
                                             <select class="form-control mb-md" name="ngo_register_city" id="ngo_register_city">
                                                <option value="">---</option>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Date Of Registration</label>
                                            <input value="{{date('d-m-Y',strtotime($ngo->registration_date))}}" style="border-radius: 0px !important;" type="text" class="form-control datepicker"  name="reg_date" id="reg_date" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Copy Of Registration Certificate</label>
                                            <select class="form-control mb-md" name="registration_certificate" id="registration_certificate" >
                                                <option>---</option>
                                                <option value="Yes" <?php if ($ngo->registration_certificate == 'Yes') echo 'selected'; ?>>Yes</option>
                                                <option value="No" <?php if ($ngo->registration_certificate == 'No') echo 'selected'; ?>>No</option>
                                            </select>
                                            <input type="file" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="registration_name" id="registration_name" style="display:none;">
                                            @if(!empty($ngo->registration_dir))
												<a href="{{URL::asset($ngo->registration_dir.$ngo->registration_name)}}">{{$ngo->registration_name}}</a>
                                            @endif
                                        </div>
                                        <div class="col-md-4">
                                            <label>Copy Of Pan Card</label>
                                            <select class="form-control mb-md" name="pan" id="pan">
                                                <option>---</option>
                                                <option value="Yes" <?php if ($ngo->pan == 'Yes') echo 'selected'; ?>>Yes</option>
                                                <option value="No" <?php if ($ngo->pan == 'No') echo 'selected'; ?>>No</option>
                                            </select>
                                            <input type="file" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="pan_name" id="pan_name"  style="display:none;">
											@if(!empty($ngo->pan_dir))
												<a href="{{URL::asset($ngo->pan_dir.$ngo->pan_name)}}">{{$ngo->pan_name}}</a>
                                            @endif
                                        </div>

                                        <!-- <div class="col-md-4">
                                            <label>Act Name. *</label>
                                            <input type="text" value="{{$ngo->act_name}}" data-msg-="Please enter your name." maxlength="100" class="form-control" name="act_name" ng_model="ngo.act_name" 
                                            id="act_name" >
                                        </div> -->
                                    </div>
                                    
                                    <h4>2. Management Members </h4>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-condensed mb-none">
                                                    <thead>
                                                        <tr>
															<th width="5%">&nbsp;</th>
                                                            <th>Name</th>
                                                            <th>Designation</th>
                                                            <th>Pan</th>
                                                            <th>Adhaar</th>
                                                            <th>Email</th>
                                                            <th>Mobile Number</th>
                                                            <th>Profile Upload</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbltable">
														@if(!empty($ngo->ngoMembers))
															@foreach($ngo->ngoMembers as $key=>$member)
																<tr id="row{{$key}}">
																	<td><button class="btn btn-default" type="button" onclick="remove_row({{$key}})">-</button></td>
																	<td>
																		<input type="text" value="{{$member->name}}" class="form-control" name="member_name[]">
																	</td>
																	<td><input type="text" value="{{$member->designation}}" class="form-control" name="member_designation[]"></td>
																	<td><input type="text" value="{{$member->pan}}" class="form-control" name="member_pan[]"></td>
																	<td><input type="text" value="{{$member->aadhar}}" class="form-control" name="member_aadhar[]"></td>
																	<td><input type="text" value="{{$member->email}}" class="form-control" name="member_email[]"></td>
																	<td><input type="text" value="{{$member->mobile}}" class="form-control" name="member_mobile[]"></td>
																	<td>
																		<input type="file" value="" class="form-control" name="member_profile_upload[]">
																		@if(!empty($member->profile_dir))
																			<a href="{{URL::asset($member->profile_dir.$member->profile)}}">{{$member->profile}}</a>
																		@endif
																	</td>
																</tr>
															@endforeach
														@endif
													</tbody>
                                                    <tfoot>
														<tr>
															<td colspan="8"><button type="button" class="btn btn-default" onclick="add_row()">+</button></td>
														</tr>
													</tfoot>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>

                                    <h4>3. Social Welfare Sector</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Focused Social Causes</label><br />
                                            <select class="form-control multiselect focused_social_causes" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }'>
                                                @foreach ($categories as $category)
                                                <optgroup label="{{ $category->category_name }}">
                                                    @foreach ($category->subCategories as $need)
                                                    <?php
                                                        $selected = '';
                                                        $array = explode(',',$ngo->focused_social_causes);
                                                        if(in_array($need->id, $array)) {
                                                            $selected = 'selected';
                                                        }
                                                    ?>
                                                    <option value="{{ $need->id }}" {{$selected}}>{{ $need->sub_category_name }}</option>
                                                    @endforeach
                                                </optgroup>
                                                @endforeach
                                            </select>
                                            <input type="hidden" value="{{$ngo->focused_social_causes}}" class="form-control" id="focused_social_causes" name="focused_social_causes">
                                        </div>

                                        <div class="col-md-4">
											<label>Operation Area</label><br />
											<select class="form-control multiselect operation_area_state" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }'>
											@foreach ($operational_states as $states)
												<?php
													$selected = '';
													$array = explode(',',$ngo->operation_area_state);
													if(in_array($states->id, $array)) {
														$selected = 'selected';
													}
												?>
												 <option value="{{ $states->id }}" {{$selected}}>{{ $states->state_name }}</option>
											@endforeach
											</select>
											<input type="hidden" value="" class="form-control" id="operation_area_state" name="operation_area_state">
                                            <!--<select class="form-control mb-md" 
                                             name="operation_area_city" id="operation_area_city">
												<option value="">---</option>
											</select>-->
                                        </div>
                                        <div class="col-md-4">
                                            <label>No Of Volunteer</label>
                                            <input type="text" value="{{$ngo->volunteer_no}}" data-msg-="Please enter your name." maxlength="100" class="form-control" name="volunteer_no" id="volunteer_no">
                                        </div>
                                    </div>

									<h4>4. Fund Availability</h4>
                                    <div class="form-group">
										<div class="col-md-4">
                                            <label>Funds Status</label>
                                            <select class="form-control mb-md" 
                                                name=" fund_status">
                                                <option>---</option>
                                                <option value="Available" @if($ngo->fund_status == 'Available') selected @endif>Available</option>
                                                <option value="Adequate" @if($ngo->fund_status == 'Adequate') selected @endif>Not Adequate</option>
                                                <option value="Not Available" @if($ngo->fund_status == 'Not Available') selected @endif>Not Available</option>
                                            </select>
                                        </div>
                                    </div>

                                    <h4>5. FCRA Details.</h4>
                                    <div class="form-group">
										<div class="col-md-4">
                                            <label>FCRA Available</label>
                                            <select class="form-control mb-md" name="fcra" id="fcra">
                                                <option>---</option>
                                                <option value="Yes" @if($ngo->fcra == 'Yes') selected @endif>Yes</option>
                                                <option value="No" @if($ngo->fcra == 'No') selected @endif>No</option>
                                            </select>
                                        </div>
										<div class="col-md-4 fcra" style="display:none;">
                                            <label>FCRA Registraton No</label>
                                            <input type="text" value="{{$ngo->fcra_reg_no}}" data-msg-="Please enter your email address." data-msg-email="Please enter a registraton no." maxlength="100" class="form-control" name="fcrano" id="fcrano">
                                        </div>
                                    </div>

									<h4>6. Contact Details</h4>
                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>Address *</label>
                                            <input type="text" value="{{$ngo->address}}" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="address" id="address">
                                        </div>
                                         <div class="col-md-4">
                                            <label>State</label>
                                            <select class="form-control mb-md" name="state" id="state">
												<option value="">---</option>
												@foreach($state as $value)
													<option value="{{$value->id}}" @if($ngo->state == $value->id) selected @endif>{{$value->state_name}}</option>
												@endforeach
											</select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City</label>
                                             <select class="form-control mb-md" name="city" id="city">
												<option value="">---</option>
											</select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>Telephone No</label>
                                             <input type="text" value="{{$ngo->telephone}}" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="telephone" id="telephone" name="telephone">
                                        </div>
                                         <div class="col-md-4">
                                            <label>Mobile No</label>
                                            <input type="text" value="{{$ngo->mobile}}" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="mobile" id="mobile">
                                        </div>
                                         <div class="col-md-4">
                                            <label>URL Website</label>
                                            <input type="text" value="{{$ngo->website_url}}" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="website_url" id="website_url">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>E-mail</label>
                                            <input type="email" value="{{$ngo->email}}" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email">
										</div>
									</div>
									</br>
                                    <div class="col-md-12">
                                        <input type="submit" value="Submit" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
                                    <!-- </div> -->
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
<script src="{{URL::asset('public/js/validation/app.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script>
	var index = '<?php echo count($ngo->ngoMembers); ?>';
		
	jQuery(document).ready(function () {
		//App.init();
        //FormValidation.init();
        $('.multiselect').multiselect();
         // $('.multiselects').multiselect();
         
        $('.datepicker').datepicker({
			format: 'dd-mm-yyyy',
		});
		$('.datepicker').datepicker('setStartDate', "01-01-1900");
        
        $('#registration_certificate').change(function() {
            if($(this).val() == 'Yes') {
                $('#registration_name').show();
            } else {
                $('#registration_name').hide();
            }
        });
        $('#registration_certificate').trigger('change');
        
        $('#pan').change(function() {
            if($(this).val() == 'Yes') {
                $('#pan_name').show();
            } else {
                $('#pan_name').hide();
            }
        });
        $('#pan').trigger('change');

        $('.datepicker').datepicker({
			format: 'dd-mm-yyyy'
		});
		
		$('.operation_area_state').change(function() {
			$('#operation_area_state').val($('.operation_area_state').val());
		});
		
		$('#ngo_register_state').trigger('change');
        $('#ngo_register_city').val('{{$ngo->ngo_register_city}}');
	
        $('#state').trigger('change');
        $('#city').val('{{$ngo->city}}');
       
        $('#fcra').change(function() {
            if($(this).val() == 'Yes') {
                $('.fcra').show();
            } else {
				$('#fcrano').val('');
                $('.fcra').hide();
            }
        });
        $('#fcra').trigger('change');
        
        $('.focused_social_causes').change(function() {
            $('#focused_social_causes').val($('.focused_social_causes').val());
        });
	});
		
		
	function formsubmit(form) {
		//$('#loading').show();
		form.submit();
	}
	
	function add_row() {
		var html = '';
		html += '<tr id="row'+index+'">';
			html += '<td>';
				html += '<button class="btn btn-default" type="button" onclick="remove_row('+index+')">-</button>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_name[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_designation[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_pan[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_aadhar[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_email[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_mobile[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="file" value="" class="form-control" name="member_profile_upload[]">';
			html += '</td>';
		html += '</tr>';
		$("#tbltable").append(html);
		$('.multiselects').multiselect();
		index++;
	}
	
	function remove_row(i) {
		var r = confirm("Do you want to remove this row ?");
		if (r == true) {
			$("#row"+i).remove();
		}
	}
	
	$('#ngo_register_state').change(function(){
		var value = $(this).val();
		$("#ngo_register_city option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                async:false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#ngo_register_city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#ngo_register_city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});
	
	/*$('#operation_area_state').change(function(){
		var value = $(this).val();
        alert(value);
		$("#operation_area_city option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#operation_area_city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#operation_area_city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});*/
	
	$('#state').change(function(){
		var value = $(this).val();
		$("#city option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                async:false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});
	
</script>
@endsection
