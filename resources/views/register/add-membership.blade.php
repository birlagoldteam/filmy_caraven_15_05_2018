<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
<script>
	var index = {{count($user->family_members)}}+1;
	$(document).ready(function () {
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '-3d'
		});
	});
	
	function add_row() {
		var html = '';
		html += '<tr id="row'+index+'">';
			html += '<td>';
				html += '<button class="btn btn-default" type="button" onclick="remove_row('+index+')">-</button>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_name[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_age[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_relationship[]">';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_employment[]">';
			html += '</td>';
			html += '<td>';
				html += '<label class="checkbox-inline">';
					html += '<input type="radio" id="inlineCheckbox1'+index+'" name="handicapped['+index+']" value="Yes"> Yes';
				html += '</label>';
				html += '<label class="checkbox-inline">';
					html += '<input type="radio" id="inlineCheckbox1'+index+'" name="handicapped['+index+']" value="No"> No';
				html += '</label>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_long_trem_disease[]">';
			html += '</td>';
		html += '</tr>';
		$("#tbltable").append(html);
		index++;
	}
	
	function remove_row(i) {
		$("#row"+i).remove();
	}
</script>
<div class="container">

	<div class="row">
		<div class="col-md-12">
			<form id="membershipForm" action="{{ url('membership-create') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" value="Contact Form" name="subject" id="subject">
			
				<div class="row">
					<h3>1. Personal Info.</h3>
					<div class="form-group">
						<div class="col-md-4">
							<label>Name *</label>
							<input type="text" value="{{$user->name}}" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name">
						</div>
						<div class="col-md-4">
							<label>Email *</label>
							<input type="email" value="{{$user->email}}" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email">
						</div>
						<div class="col-md-4">
							<label>Mobile *</label>
							<input type="text" value="{{$user->mobile}}" data-msg-required="Please enter your Mobile." maxlength="100" class="form-control" name="mobile" id="mobile">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
						{{$user->gender}}
							<label>Gender *</label>
							<select class="form-control mb-md" default="{{$user->gender}}">
								<option>---</option>
								<option value="Male">Male</option>
								<option value-="Female">Female</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>Date of Birth *</label>
							<input type="text" value="{{$user->date_of_birth}}" class="form-control datepicker" name="date_of_birth" id="date_of_birth">
						</div>
						<div class="col-md-4">
							<label>Aadhar No. *</label>
							<input type="text" value="{{$user->aadhar_number}}" data-msg-required="Please enter your Adhar No." maxlength="100" class="form-control" name="aadhar" id="aadhar">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<label>PAN *</label>
							<input type="text" value="{{$user->pan}}" data-msg-required="Please enter your PAN." maxlength="100" class="form-control" name="pan" id="pan">
						</div>
						<div class="col-md-4">
							<label>Marital Status</label>
							<select class="form-control mb-md">
								<option>---</option>
								<option value="Male">Married</option>
								<option value-="Female">Single</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>Children *</label>
							<input type="text" value="{{$user->children}}" data-msg-required="Please enter Total No. of Childrens." maxlength="100" class="form-control" name="children" id="children">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<label>Blood Group *</label>
							<input type="text" value="{{$user->blood_group}}" data-msg-required="Please enter your Blood Group." maxlength="100" class="form-control" name="blood_group" id="blood_group">
						</div>
					</div>
				</div>
				<div class="row">
					<h5>Address</h5>
					<div class="form-group">
						<div class="col-md-4">
							<label>Country *</label>
							<select class="form-control mb-md">
								<option>---</option>
								<option value="1">India</option>
								<option value-="2">Australia</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>State *</label>
							<select class="form-control mb-md">
								<option>---</option>
								<option value="1">Maharashtra</option>
								<option value-="2">Rajasthan</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>City/District *</label>
							<select class="form-control mb-md">
								<option>---</option>
								<option value="1">Thane</option>
								<option value-="2">Mumbai</option>
							</select>
						</div>
					</div>
					<div class="form-group">	
						<div class="col-md-4">
							<label>Tehsil *</label>
							<select class="form-control mb-md">
								<option>---</option>
								<option value="1">Kalyan</option>
								<option value-="2">Palghar</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>Gram Panchayat *</label>
							<select class="form-control mb-md">
								<option>---</option>
								<option value="1">Rasal</option>
								<option value-="2">Pali</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>Village *</label>
							<select class="form-control mb-md">
								<option>---</option>
								<option value="1">Bhiwandi</option>
								<option value-="2">Dombivli</option>
							</select>
						</div>
					</div>
					<div class="form-group">	
						<div class="col-md-4">
							<label>Stree/Area *</label>
							<input type="text" value="{{$user->street_area}}" data-msg-required="Please enter Street Name" maxlength="100" class="form-control" name="street_name" id="street_name">
						</div>
						<div class="col-md-4">
							<label>House Name/Number *</label>
							<input type="text" value="{{$user->house_name_no}}" data-msg-required="Please enter your Home name/Number." class="form-control" name="house" id="house">
						</div>
						<div class="col-md-4">
							<label>Pincode *</label>
							<input type="text" value="{{$user->pincode}}" data-msg-required="Please enter your Pincode." maxlength="100" class="form-control" name="pincode" id="pincode">
						</div>
					</div>
				</div>

				<div class="row">
					<h5>Education</h5>
					<div class="form-group">
						<div class="col-md-4">
							<label>Country *</label>
							<select class="form-control mb-md" name="education_country" id="education_country">
								<option>---</option>
								<option value="1">India</option>
								<option value-="2">Australia</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>Language Known *</label>
							<input type="text" value="{{$user->aadhar_number}}" data-msg-required="Please enter your Languages you Know." class="form-control" name="language_known" id="language_known">
						</div>
					</div>
				</div>

				<div class="row">
					<h5>Employment</h5>
					<div class="form-group">
						<div class="col-md-4">
							<label>Country *</label>
							<select class="form-control mb-md" name="education_country" id="education_country">
								<option>---</option>
								<option value="1">India</option>
								<option value-="2">Australia</option>
							</select>
						</div>
						<div class="col-md-4">
							<label>Other *</label>
							<input type="text" value="" data-msg-required="Others." maxlength="100" class="form-control" name="others" id="others">
						</div>
					</div>
				</div>

				<div class="row">
					<h5>Health Profile</h5>
					<div class="form-group">
						<div class="col-md-4">
							<label>Handicapped *</label>
							<input type="text" value="" class="form-control" name="handicapped" id="handicapped">
						</div>
						<div class="col-md-4">
							<label>Any Long-Term Diseases  *</label>
							<input type="text" value="" class="form-control" name="long_term_disease" id="long_term_disease">
						</div>
					</div>
				</div>
			
				<div class="row">
					<div class="row">
						<h3>2. Family Info.</h3>
						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none">
										<thead>
											<tr>
												<th width="5%">&nbsp;</th>
												<th width="20%">Name</th>
												<th width="5%">Age</th>
												<th width="20%">Relationship</th>
												<th width="10%">Employment</th>
												<th width="20%">Handicapped</th>
												<th width="20%">Any Long-Term Diseases</th>
											</tr>
										</thead>
										<tbody id="tbltable">
											@if(!empty($user->family_members))
												@foreach($user->family_members as $key=>$family)
													<tr>
														<td><button class="btn btn-default" type="button" onclick="remove_row({{$key}})">-</button></td>
														<td><input type="text" value="{{$family->family_name}}" class="form-control" name="family_name[]"></td>
														<td><input type="text" value="{{$family->family_age}}" class="form-control" name="family_age[]"></td>
														<td><input type="text" value="{{$family->family_relationship}}" class="form-control" name="family_relationship[]"></td>
														<td><input type="text" value="{{$family->family_employment}}" class="form-control" name="family_employment[]"></td>
														<td class="text-right">
															<label class="checkbox-inline">
																<input type="radio" id="inlineCheckbox1'+index+'" name="handicapped[{{$key}}]" value="Yes"> Yes
															</label>
															<label class="checkbox-inline">
																<input type="radio" id="inlineCheckbox1'+index+'" name="handicapped[{{$key}}]" value="No"> No
															</label>
														</td>
														<td><input type="text" value="{{$family->family_long_trem_disease}}" class="form-control" name="family_long_trem_disease[]"></td>
													</tr>
												@endforeach
											@endif
										</tbody>
										<tfoot>
											<tr>
												<td colspan="7"><button type="button" class="btn btn-default" onclick="add_row()">+</button></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>	
						</div>
					</div>
				</div>	

				<div class="row">
					<div class="row">
						<h3>3. Income Info.</h3>
						<div class="form-group">
							<div class="col-md-4">
								<label>Type of Work *</label>
								<select class="form-control mb-md">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-md-4">
								<label>Income *</label>
								<select class="form-control mb-md">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-md-4">
								<label>Vehicle *</label>
								<select class="form-control mb-md">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<label>House *</label>
								<select class="form-control mb-md">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-md-4">
								<label>Loan *</label>
								<select class="form-control mb-md">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-md-4">
								<label>Insurance *</label>
								<select class="form-control mb-md">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="row">
						<h3>4. Social Welfare</h3>
						<div class="form-group">
							<div class="col-md-12">
								<label>Social Welfare Needs *</label></div>
								<div class="col-md-12">
									<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox1" value="true" name="education" <?php if ($user->social_education == true) echo 'checked'; ?>> Education
									</label>
									<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox2" value="true" name="water_sanitation" <?php if ($user->water_and_sanitation == true) echo 'checked'; ?>> Water & Sanitation
									</label>
									<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox3" value="true" name="healthcare" <?php if ($user->healthcare == true) echo 'checked'; ?>> Healthcare
									</label>
									<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox4" value="true" name="environmental" <?php if ($user->environmental == true) echo 'checked'; ?>> Environmental
									</label>
									<label class="checkbox-inline">	
										<input type="checkbox" id="inlineCheckbox5" value=true name="economic_empowerment" <?php if ($user->social_economic_empowerment == true) echo 'checked'; ?>> Social & Economic Empowerment
									<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox6" value="true" name="sports_culture" <?php if ($user->sports_and_culture == true) echo 'checked'; ?>> Sports & Culture
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="row">
					<div class="col-md-12">
						<input type="submit" value="Register" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
