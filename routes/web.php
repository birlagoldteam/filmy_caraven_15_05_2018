<?php

Auth::routes();
Route::get('/', function () {
	return view('welcome');
})->name('welcome');

Route::get('/', function () {
	return view('welcome');
})->name('welcome');

Route::get('about-us', function () {
	return view('website.about');
})->name('about');

Route::get('what-we-do', function () {
	return view('website.whatwedo');
})->name('whatwedo');

Route::get('management-team', function () {
	return view('website.managementteam');
})->name('managementteam');

Route::get('production', function () {
	return view('website.production');
})->name('production');

Route::get('distribution', function () {
	return view('website.distribution');
})->name('distribution');

Route::get('film-exhibition', function () {
	return view('website.filmexhibition');
})->name('filmexhibition');

Route::get('filmy-caravan', function () {
	return view('website.filmycaravan');
})->name('filmycaravan');

Route::get('opportunity', function () {
	return view('website.opportunity');
})->name('opportunity');

Route::get('contact-us', function () {
	return view('website.contact');
})->name('contact');


Route::post('get-film', function () {
	//return view('');
	dd("asdf");
})->name('getfilm');


/*Route::get('member-registration', function () {
	return view('member.memberregister');
})->name('memberregister');


*/



Route::get('/home', 'HomeController@index')->name('home');

Route ::get('memberregister',[
	'uses'=> 'Auth\RegisterController@memberregister',
	'as'=>  'memberregister',
]);

Route::post('check-otp',[
	'uses' =>'Auth\RegisterController@otp', 
	'as' =>'checkotp',
]);

Route::post('member-register',[
	'uses' =>'Auth\RegisterController@register', 	
	'as' =>'member-register-post',
]);


Route::get('membership-registration',[
	'uses'=>'MembershipsController@membershipRegistration',
	'as'=>'membershipregistration',
	'middleware'=>'auth'
]);


Route::post('membership-create', [
    'uses' => 'MembershipsController@membershipCreate',
    'as' => 'membership-create',
]);

Route::get('get-district', [
    'uses' => 'MembershipsController@getDistricts',
    'as' => 'getdistrict',
    //'middleware'=>'auth'
]);

Route::get('get-tehsil', [
    'uses' => 'MembershipsController@getTehsils',
    'as' => 'gettehsil',
    //'middleware'=>'auth'
]);

Route::get('get-grampanchayat', [
    'uses' => 'MembershipsController@getgrampanchayats',
    'as' => 'getgrampanchayat',
    //'middleware'=>'auth'
]);

Route::get('membership-updation','MembershipsController@membershipUpdation')->name('membership-updation')->middleware('auth');
Route::post('membership-update', [
	'uses' => 'MembershipsController@membershipUpdate',
	'as' => 'membership-update',
	'middleware'=>'auth'
]);







Route::group(['prefix' => 'api'], function () {
    Route::post('/generateotp','TestController@generateotp');
    Route::post('/register', 'TestController@register');
    Route::post('/memberrship-register', 'TestController@membership_register');
    Route::post('/social-issue', 'TestController@social_issue');
    Route::post('/state', 'TestController@state');
    Route::post('/district', 'TestController@district');
    Route::post('/tehsil', 'TestController@tehsil');
    Route::post('/grampanchayat', 'TestController@grampanchayat');
    Route::post('/terms-conditions', 'TestController@terms_conditions');
    Route::post('/education','TestController@education');
    Route::post('/employment','TestController@employment');
    Route::post('/vehicle','TestController@vehicle');
    Route::post('/anylongterm','TestController@anylongterm'); 
    Route::post('/landproperty','TestController@landproperty');
    Route::post('/existingloan','TestController@existingloan');
    Route::post('/insurance','TestController@insurance');
    Route::post('/relationships','TestController@getRelationships');
    Route::post('/languages','TestController@getLanguages');
    Route::post('/userupdatepro','TestController@userupdateprofile');
    Route::get('/getuserprofile','TestController@getuserprofile');
    Route::post('/declairation', 'TestController@declairation');
});


Route::group(['prefix'=>'ngo'], function () {
	//Route::get('/login', 'Auth\NgoLoginController@showloginform')->name('ngo-login');
	Route::get('login',[
		'uses'=>'Auth\NgoLoginController@showloginform',
		'as'=>'ngo-login',
		//'middleware'=>'auth'
	]);
	
	//Route::post('/login', 'Auth\NgoLoginController@login')->name('ngo-login-submit');
	Route::post('login',[
		'uses'=>'Auth\NgoLoginController@login',
		'as'=>'ngo-login-submit',
		//'middleware'=>'auth'
	]);
	
	
	//Route::get('/home', 'NgoHomeController@demo')->name('ngohome');
	Route::get('home',[
		'uses'=>'NgoHomeController@demo',
		'as'=>'ngohome',
		//'middleware'=>'auth'
	]);
	
	//Route::get('/check-email', 'NgoHomeController@checkemail')->name('ngocheckemail');
	Route::get('check-email',[
		'uses'=>'NgoHomeController@checkemail',
		'as'=>'ngocheckemail',
		//'middleware'=>'auth'
	]);
	
	//Route::get('/check-email-otp', 'NgoHomeController@checkemailotp')->name('ngocheckemailotp');
	Route::get('check-email-otp',[
		'uses'=>'NgoHomeController@checkemailotp',
		'as'=>'ngocheckemailotp',
		//'middleware'=>'auth'
	]);
	
	//Route::get('/forgot-password', 'NgoHomeController@forgotpassword')->name('forgotpassword');
	Route::get('forgot-password',[
		'uses'=>'NgoHomeController@forgotpassword',
		'as'=>'forgotpassword',
		//'middleware'=>'auth'
	]);
	
	//Route::post('/reset-password', 'NgoHomeController@resetpassword')->name('resetpassword');
	Route::post('reset-password',[
		'uses'=>'NgoHomeController@resetpassword',
		'as'=>'resetpassword',
		//'middleware'=>'auth'
	]);
	//Route::get('dashboard', 'NgoHomeController@dashboard')->name('ngodashboard');
	Route::get('dashboard',[
		'uses'=>'NgoHomeController@dashboard',
		'as'=>'ngodashboard',
		'middleware'=>'auth'
	]);
	
	Route::get('ngo-add-details',[
		'uses'=>'NgoDetailsController@addNgo',
		'as'=>'addngodetails',
		//'middleware'=>'auth'
	]);

	Route::get('ngo-update-details',[
		'uses'=>'NgoDetailsController@updateNgo',
		'as'=>'updatengodetails',
		//'middleware'=>'auth'
	]);

	Route::post('add-ngo', [
		'uses' => 'NgoDetailsController@add_ngo',
		'as' => 'add_ngo',
		//'middleware'=>'auth'
	]);

	Route::post('update-ngo', [
		'uses' => 'NgoDetailsController@update_ngo',
		'as' => 'update_ngo',
		//'middleware'=>'auth'
	]);
	
	Route::get('ngo-registration', [
		'uses' => 'NgoHomeController@demo',
		'as' => 'ngo-registration',	
	]);

	Route::post('otpngoverify', [
		'uses' => 'NgoHomeController@otpngoverify',
		'as' => 'otpngoverify',
		//'middleware'=>'auth'
		
	]);


	Route::post('emailvalidate', [
		'uses' => 'NgoHomeController@emailvalidatengo',
		'as' => 'emailvalidate',
		
	]);

	Route::post('addngo-register', [
		'uses' => 'NgoHomeController@addngoregister',
		'as' => 'addngo-register',
		
	]);
});

Route::group(['prefix'=>'admin'], function () {

	Route::get('{url}', 'Admin\UserController@showloginform')->name('admin-login')->where('url', '/|login');
	
	/* Admin Login Routes */

	Route::get('/', 'Admin\UserController@logout')->name('admin-logout');
	Route::post('/admin-login', 'Admin\UserController@admin_login')->name('admin-login-submit');
	
	Route::get('dashboard','Admin\UserController@dashboard')->name('admindashboard');
	
	
	/* Admin States Routes */
	Route::get('states', 'Admin\StateController@index')->name('adminstates');
	//Route::get('states', 'Admin\StateController@edit')->name('adminstates');
	
	Route::any('state-edit/{id?}', [
        'uses' => 'Admin\StateController@edit',
        'as' => 'state-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('state-delete/{id?}', [
        'uses' => 'Admin\StateController@delete',
        'as' => 'state-delete',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/admin-update-state', 'Admin\StateController@updateState')->name('admin-update-state');
	
	/* Admin Districts Route */
	Route::get('districts', 'Admin\DistrictController@index')->name('admindistricts');
	Route::any('district-edit/{id?}', [
        'uses' => 'Admin\DistrictController@edit',
        'as' => 'district-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('district-delete/{id?}', [
        'uses' => 'Admin\DistrictController@delete',
        'as' => 'district-delete',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/admin-update-district', 'Admin\DistrictController@updateDistrict')->name('admin-update-district');
	
	/* Admin Tehsil Route */
	Route::get('tehsils', 'Admin\TehsilController@index')->name('admintehsils');
	Route::any('tehsil-edit/{id?}', [
        'uses' => 'Admin\TehsilController@edit',
        'as' => 'tehsil-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('tehsil-delete/{id?}', [
        'uses' => 'Admin\TehsilController@delete',
        'as' => 'tehsil-delete',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/admin-update-tehsil', 'Admin\TehsilController@updateTehsil')->name('admin-update-tehsil');
    
    /* Admin Grampanchayats Route */
	Route::get('grampanchayats', 'Admin\GrampanchayatController@index')->name('admingrampanchayats');
	Route::any('grampanchayat-edit/{id?}', [
        'uses' => 'Admin\GrampanchayatController@edit',
        'as' => 'grampanchayat-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('grampanchayat-delete/{id?}', [
        'uses' => 'Admin\GrampanchayatController@delete',
        'as' => 'grampanchayat-delete',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/admin-update-grampanchayat', 'Admin\GrampanchayatController@updateGrampanchayat')->name('admin-update-grampanchayat');
	
	/* Admin Movies Route */
	Route::get('movies', 'Admin\MovieController@index')->name('adminmovies');
	Route::any('movie-edit/{id?}', [
        'uses' => 'Admin\MovieController@edit',
        'as' => 'movie-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('movie-delete/{id?}', [
        'uses' => 'Admin\MovieController@delete',
        'as' => 'movie-delete',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/admin-update-movie', 'Admin\MovieController@updateMovie')->name('admin-update-movie');
	
	/* Movie Schedule Routes */
	Route::get('movie-schedule', 'Admin\MovieScheduleController@movieschedule')->name('adminmovieschedules');
	Route::any('movie-schedule-edit/{id?}', [
        'uses' => 'Admin\MovieScheduleController@edit',
        'as' => 'movie-schedule-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('movie-schedule-search/{date?}', [
        'uses' => 'Admin\MovieScheduleController@search',
        'as' => 'movie-schedule-search',
        'middleware' => 'auth:admin'
    ]);
	Route::any('movie-schedule-attendances/{id?}', [
        'uses' => 'Admin\MovieScheduleController@attendances',
        'as' => 'movie-schedule-attendances',
        'middleware' => 'auth:admin'
    ]);
    Route::get('movie-schedule-report-export', [
        'uses' => 'Admin\MovieScheduleController@schedulereportexport',
        'as' => 'movie-schedule-report-export',
        'middleware' => 'auth:admin'
    ]);
	Route::any('movie-schedule-delete/{id?}', [
        'uses' => 'Admin\MovieScheduleController@delete',
        'as' => 'movie-schedule-delete',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/admin-update-movie-schedule', 'Admin\MovieScheduleController@updateMovieSchedule')->name('admin-update-movie-schedule');
	
	/* Admin Categories Routes */
	Route::get('categories', 'Admin\CategoryController@index')->name('admincategories');
	
	Route::any('category-edit/{id?}', [
        'uses' => 'Admin\CategoryController@edit',
        'as' => 'category-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('category-delete/{id?}', [
        'uses' => 'Admin\CategoryController@delete',
        'as' => 'category-delete',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/admin-update-category', 'Admin\CategoryController@updateCategory')->name('admin-update-category');
	
	/* Admin Sub Categories Routes */
	Route::get('subcategories', 'Admin\SubCategoryController@index')->name('adminsubcategories');
	
	Route::any('sub-category-edit/{id?}', [
        'uses' => 'Admin\SubCategoryController@edit',
        'as' => 'sub-category-edit',
        'middleware' => 'auth:admin'
    ]);
	Route::any('sub-category-delete/{id?}', [
        'uses' => 'Admin\SubCategoryController@delete',
        'as' => 'sub-category-delete',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/admin-update-sub-category', 'Admin\SubCategoryController@updateSubCategory')->name('admin-update-sub-category');
	
});

Route::post('send-sms',[
	'uses' => 'SmsController@movieschedule',
    'as' => 'sendsms',
]);

Route::get('check',function(Illuminate\Http\Request $request){
	//app('App\Http\Controllers\Controller')->sendsms("8850901217","test");
	dd(Auth::guard()->user());	
});

Route::post('contact_us', 'ContactUsController@cont')->name('contact_us');
